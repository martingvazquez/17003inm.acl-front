Acl.controller("financingsController", function($scope, financingsModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[8].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    $scope.financingTypes = {};


    if(section == 'financings' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getFinancings('/acl/financings/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/financings/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getFinancings(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/financings/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getFinancings(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows){
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/financings/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getFinancings(urlApp,$scope,$http);
        }

        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/financings/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getFinancings(urlApp,$scope,$http);
        }

        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el financiamiento?")){
            $http.patch('/acl/financings/token/'+token+'/'+id,{status:0}).
            success(function(financingsData) {
                financings = financingsData.financings[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/financings/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getFinancings(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El financiamiento se desactivó de manera exitosa, Financiamiento: "+financings.name;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el financiamiento?")){
            $http.patch('/acl/financings/token/'+token+'/'+id,{status:1}).
            success(function(financingsData) {
                financings = financingsData.financings[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/financings/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getFinancings(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El financiamiento se activó de manera exitosa, Cliente: "+financings.name;
                showAlert($scope);

            });
          }
        }
    }

    if(section =='financings' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
      getFinancingTypes('/acl/financing-types/token/'+token+'/filter/financing_types.status/1',$scope,$http,true);
        $scope.submitForm = function(formData,financingsData) {
            $http.post('/acl/financings/token/'+token,{
              description:formData.description,
              notes:formData.notes,
              financing_type_id:formData.financing_type_id
            }).
            success(function(financingsData) {
                  financing = financingsData.financings[0];
                  status = financingsData.status
                  if(status == 'ok') {
                    $scope.alertType = "success";
                    $scope.alertText = "El financiamiento se agregó de manera exitosa, Financiamiento: "+ financing.description;
                    formData.description="";
                    formData.notes="";
                    formData.financing_type_id=[''];
                  }
                  else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar un financiamiento repetido";
                  }
                showAlert($scope);
            })
        }
    }



    if(section =='financings' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      financing_id = $routeParams.financing_id;
      urlApp = '/acl/financings/token/'+token+'/'+ financing_id;

      getFinancings(urlApp,$scope,$http);

      getFinancingTypes('/acl/financing-types/token/'+token+'/filter/financing_types.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,financingsData) {
        $http.put('/acl/financings/token/'+token+'/'+ financing_id,{
          description:formData.description,
          notes:formData.notes,
          financing_type_id:formData.financing_type_id
        }).
        success(function(financingsData) {
            $scope.financing = financingsData.financings;
            status = financingsData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "El financiamiento se actualizo de manera exitosa, Financiamiento: "+$scope.financing[0].name;
            }
            else{
             $scope.alertType = "danger";
             $scope.alertText = "No es posible ingresar un nombre repetida, Financiamiento: "+$scope.financing[0].name;

            }
          showAlert($scope);
        })
      }
    }

})
