Acl.controller("citiesController", function($scope, citiesModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;

    $scope.sectionTitle = FirstUpperCase(sec.section[1].title);

    //variables filtros
    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItemsCities = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    // variables alertas
    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    //variables tablas foraneas
    $scope.states = {};

    //funciones Generales

    $scope.changeCountries = function(country_id){
      $scope.selectState = {}
      $scope.selectState.show = true;
      $scope.optionDefault = false;
      getStates($location,'/acl/states/token/'+token+'/filters2/states.country_id/'+country_id+'/states.status/1',$scope,$http,true);
      $scope.formData.state_id=[''];
    }


    if(section == 'cities' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getCities($location,'/acl/cities/token/'+token,$scope,$http,true);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/cities/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCities($location,urlApp,$scope,$http,true);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/cities/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCities($location,urlApp,$scope,$http,true);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/cities/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCities($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/cities/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCities($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el estado?")){
            $http.patch('/acl/cities/token/'+token+'/'+id,{status:0}).
            success(function(citiesData) {
                cities = citiesData.cities[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/cities/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getCities($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La ciudad se desactivó de manera exitosa, Ciudad: "+cities.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el estado?")){
            $http.patch('/acl/cities/token/'+token+'/'+id,{status:1}).
            success(function(citiesData) {
                cities = citiesData.cities[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/cities/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getCities($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La ciudad se activó de manera exitosa, Ciudad: "+cities.description;
                showAlert($scope);

            });
          }
        }
    }
    if(section =='cities' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getCountries($location,'/acl/countries/token/'+token+'/filter/countries.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,citiesData){
            $http.post('/acl/cities/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                state_id:formData.state_id
            }).
            success(function(citiesData) {
                var error = citiesData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
                $scope.city = citiesData;
                $scope.city = citiesData.cities;
                status = citiesData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "La ciudad se agregó de manera exitosa, Ciudad: "+$scope.city[0].description;
                    formData.description="";
                    formData.notes="";
                    formData.state_id=[''];
                    formData.country_id=[''];
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una descripcion repetida";
                }
                showAlert($scope);
            })
        }
    }
    if(section =='cities' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      city_id = $routeParams.city_id;
      urlApp = '/acl/cities/token/'+token+'/'+city_id;
      getCities($location,urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,citiesData) {
          $http.put('/acl/cities/token/'+token+'/'+city_id,{
              description:formData.description,
              notes:formData.notes,
              state_id:formData.state_id
            }).
          success(function(citiesData) {
              $scope.city = citiesData.cities;
              status = citiesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "La ciudad se actualizó de manera exitosa, Ciudad: "+$scope.city[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, Ciudad: "+$scope.city[0].description;
              }
            showAlert($scope);
          })
      }
    }
})
