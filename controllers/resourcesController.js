Acl.controller("resourcesController", function($scope, resourcesModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[5].title);

    //variables filtros
    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    // variables alertas
    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    //variables tablas foraneas
    $scope.applications = {};

    if(section == 'resources' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getResources($location,'/acl/resources/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/resources/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getResources($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/resources/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getResources($location,urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/resources/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getResources($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/resources/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getResources($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el recurso?")){
            $http.patch('/acl/resources/token/'+token+'/'+id,{status:0}).
            success(function(resourcesData) {
                resources = resourcesData.resources[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/resources/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getResources($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El recurso se desactivó de manera exitosa, Recurso: "+resources.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el recurso?")){
            $http.patch('/acl/resources/token/'+token+'/'+id,{status:1}).
            success(function(resourcesData) {
                resources = resourcesData.resources[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/resources/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getResources($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El recurso se activó de manera exitosa, Recurso: "+resources.description;
                showAlert($scope);

            });
          }
        }
    }
    if(section =='resources' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getApplications($location,'/acl/applications/token/'+token+'/filter/applications.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,resourcesData) {
            $http.post('/acl/resources/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                application_id:formData.application_id
            }).
            success(function(resourcesData) {
                var error = resourcesData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
                $scope.resource = resourcesData;
                $scope.resource = resourcesData.resources;
                status = resourcesData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El recurso se agregó de manera exitosa, Recurso: "+$scope.resource[0].description;
                    formData.description="";
                    formData.notes="";
                    formData.application_id='';
                }
                else{
                  error = resourcesData.error;
                  $scope.alertType = "danger";
                  $scope.alertText = resourcesData.message;
                }
                showAlert($scope);
            })
        }
    }
    if(section =='resources' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      resource_id = $routeParams.resource_id;
      urlApp = '/acl/resources/token/'+token+'/'+resource_id;
      getResources($location,urlApp,$scope,$http);
      getApplications($location,'/acl/applications/token/'+token+'/filter/applications.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,resourcesData) {
          $http.put('/acl/resources/token/'+token+'/'+resource_id,{
              description:formData.description,
              notes:formData.notes,
              application_id:formData.application_id
            }).
          success(function(resourcesData) {
              $scope.resource = resourcesData.resources;
              status = resourcesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El recurso se actualizó de manera exitosa, Recurso: "+$scope.resource[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, Recurso: "+$scope.resource[0].description;
              }
            showAlert($scope);
          })
      }
    }
})
