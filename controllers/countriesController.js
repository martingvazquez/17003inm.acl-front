Acl.controller("countriesController", function($scope, countriesModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[3].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";




    if(section == 'countries' && action == 'states'){
      urlApp = '/acl/states/token/'+token+'/filter/states.country_id/'+$routeParams.country_id;
      getStates($location,urlApp,$scope,$http,true);

      urlApp = '/acl/countries/token/'+token+'/'+$routeParams.country_id;
      getCountry($location,urlApp,$scope,$http);


      $scope.addSubmitForm = function(formData,statesData) {
          $http.post('/acl/states/token/'+token,{
              description:formData.description,
              notes:formData.notes,
              country_id:$routeParams.country_id
          }).
          success(function(statesData) {
              var error = statesData.error;
              if(error == 4){
                  $location.path('/login/denegado');
              }
              $scope.state = statesData;
              $scope.state = statesData.states;
              status = statesData.status;
              if(status == 'ok'){
                  $scope.alertType = "success";
                  $scope.alertText = "El estado se agregó de manera exitosa, Estado: "+$scope.state[0].description;
                  formData.description="";
                  formData.notes="";

                  urlApp = '/acl/states/token/'+token+'/filter/states.country_id/'+$routeParams.country_id;
                  getStates($location,urlApp,$scope,$http,true);

              }
              else{
                error = statesData.error;
                $scope.alertType = "danger";
                $scope.alertText = statesData.mensaje;
              }
              showAlert($scope);
          })
      }
    }



    if(section == 'countries' && action == 'cities'){
      urlApp = '/acl/cities/token/'+token+'/filter/cities.state_id/'+$routeParams.state_id;
      getCities($location,urlApp,$scope,$http,true);

      urlApp = '/acl/states/token/'+token+'/'+$routeParams.state_id;
      getState($location,urlApp,$scope,$http);


      $scope.addSubmitForm = function(formData,citiesData) {
          $http.post('/acl/cities/token/'+token,{
              description:formData.description,
              notes:formData.notes,
              state_id:$routeParams.state_id
          }).
          success(function(citiesData) {
              var error = citiesData.error;
              if(error == 4){
                  $location.path('/login/denegado');
              }
              $scope.city = citiesData;
              $scope.city = citiesData.cities;
              status = citiesData.status;
              if(status == 'ok'){
                  $scope.alertType = "success";
                  $scope.alertText = "La ciudad se agregó de manera exitosa, Ciudad: "+$scope.city[0].description;
                  formData.description="";
                  formData.notes="";

                  urlApp = '/acl/cities/token/'+token+'/filter/cities.state_id/'+$routeParams.state_id;
                  getCities($location,urlApp,$scope,$http,true);

              }
              else{
                error = citiesData.error;
                $scope.alertType = "danger";
                $scope.alertText = citiesData.mensaje;
              }
              showAlert($scope);
          })
      }
    }


    if(section == 'countries' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getCountries($location,'/acl/countries/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/countries/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCountries($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/countries/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCountries($location,urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/countries/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCountries($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/countries/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCountries($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el país?")){
            $http.patch('/acl/countries/token/'+token+'/'+id,{status:0}).
            success(function(countriesData) {
                countries = countriesData.countries[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/countries/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getCountries($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El país se desactivó de manera exitosa, País: "+countries.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el país?")){
            $http.patch('/acl/countries/token/'+token+'/'+id,{status:1}).
            success(function(countriesData) {
                countries = countriesData.countries[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/countries/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getCountries($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El país se activó de manera exitosa, País: "+countries.description;
                showAlert($scope);

            });
          }
        }
    }
    if(section =='countries' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,countriesData) {
            $http.post('/acl/countries/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(countriesData) {
              var error = countriesData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
              $scope.country = countriesData;
              $scope.country = countriesData.countries;
              status = countriesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El país se agregó de manera exitosa, País: "+$scope.country[0].description;
                formData.description="";
                formData.notes="";
              }
              else{
                // $scope.alertType = "danger";
                // $scope.alertText = "No es posible agregar una descripcion repetida";

                error = countriesData.error;
                $scope.alertType = "danger";
                $scope.alertText = countriesData.message;

              }
            showAlert($scope);
            })
        }
    }
    if(section =='countries' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      country_id = $routeParams.country_id;
      urlApp = '/acl/countries/token/'+token+'/'+country_id;
      getCountries($location,urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,countriesData) {
          $http.put('/acl/countries/token/'+token+'/'+country_id,{description:formData.description,notes:formData.notes}).
          success(function(countriesData) {
              $scope.country = countriesData.countries;
              status = countriesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El país se actualizó de manera exitosa, País: "+$scope.country[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, País: "+$scope.countrie[0].description;
              }
            showAlert($scope);
          })
      }
    }
})
