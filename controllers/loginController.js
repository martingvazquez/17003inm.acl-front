Acl.controller("loginController", function($scope,$rootScope,$http,$location,$routeParams){
  token = getToken();
  if(!token){
      $location.path("/");
  }
  var url = $location.url();

  urlSplit=splitUrl(url);

  section = urlSplit[1];
  action = urlSplit[2];

  var sec = JSON.parse(sections);
  var act = JSON.parse(actions);

  $scope.section = section;

  $scope.sectionTitle = FirstUpperCase(sec.section[9].title);


  // variables alertas
  $scope.alertMessage = null;
  $scope.alertType = "success";
  $scope.secondsDelay = 5;
  $scope.closable = true;
  $scope.alertText = "";


  if($scope.section==""){
    $scope.section   =   'Login';
    $scope.action    =   'Inicio';

    section = 'login';
    action = 'inicio';
  }

  if(action == 'bienvenido'){
    $("#inm-adm").addClass("not-visible");
  }

  if(section =='login' && action == 'denegado'){
    $scope.alertType = "danger";
    $scope.alertText = "Acceso denegado";
    showAlert($scope);
  }

  if(section =='login' && action == 'salir'){

    var url = '/acl/sessions/'+token;

    $http.delete(url).success(function(sessionsData) {
      error = sessionsData.error;
      if(error == 0){
        document.cookie =   'token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        $rootScope.token=false;
        location.reload();
        $location.path("/");
      }
    });
  }

  if(section =='login' && action == 'inicio'){
      if(!token){
        $scope.addSubmitForm = function(formData) {
          user = formData.user;
          password = formData.password;
          var url = '/acl/sessions/valid/'+user+'/'+password;

          $http.get(url).success(function(sessionsData) {

            var error = sessionsData.error;

            if(error>=1){
              var mensaje = 'Usuario o contraseña no validos';
              $scope.alertType = "danger";
              $scope.alertText = mensaje;
            }
            else{
              var mensaje = 'Inicio correcto';
              $scope.alertType = "success";
              $scope.alertText = mensaje;
              token = sessionsData.token;
              document.cookie = "token="+token;
              $rootScope.token=true;
              location.reload();
              $location.path("/login/bienvenido");
            }
            showAlert($scope);
          });
      }
    }
    else{
      $location.path("/login/bienvenido");
    }
  }

  if(section =='login' && action == 'bienvenido'){
    $scope.token = token;
  }
})
