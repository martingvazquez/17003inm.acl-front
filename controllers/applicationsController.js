Acl.controller("applicationsController", function($scope, applicationsModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    var url = $location.url();
    urlSplit=splitUrl(url);
    app = urlSplit[1];
    section = urlSplit[2];
    action = urlSplit[3];

    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;

    $scope.sectionTitle = FirstUpperCase(sec.section[0].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    if(section == 'applications' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getApplications($location, '/acl/applications/token/'+token,$scope,$http,true);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/applications/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getApplications($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/applications/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getApplications($location,urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/applications/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getApplications($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/applications/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getApplications($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el país?")){
            $http.patch('/acl/applications/token/'+token+'/'+id,{status:0}).
            success(function(applicationsData) {
                applications = applicationsData.applications[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/applications/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getApplications($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La aplicación se desactivó de manera exitosa, Aplicación: "+applications.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el país?")){
            $http.patch('/acl/applications/token/'+token+'/'+id,{status:1}).
            success(function(applicationsData) {
                applications = applicationsData.applications[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/applications/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getApplications($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La aplicación  se activó de manera exitosa, Aplicación: "+applications.description;
                showAlert($scope);

            });
          }
        }
    }
    if(section =='applications' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,applicationsData) {
            $http.post('/acl/applications/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                port:formData.port
              }).
            success(function(applicationsData) {
              var error = applicationsData.error;
              if(error == 4){
               $location.path('/login/denegado');
              }
              $scope.application = applicationsData;
              $scope.application = applicationsData.applications;
              status = applicationsData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "La aplicación se agregó de manera exitosa, Aplicación: "+$scope.application[0].description;
                formData.description="";
                formData.notes="";
                formData.port = "";
              }
              else{
                // $scope.alertType = "danger";
                // $scope.alertText = "No es posible agregar una descripcion repetida";

                error = applicationsData.error;
                $scope.alertType = "danger";
                $scope.alertText = applicationsData.message;

              }
            showAlert($scope);

            })
        }
    }
    if(section =='applications' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      application_id = $routeParams.application_id;
      urlApp = '/acl/applications/token/'+token+'/'+application_id;
      getApplications($location,urlApp,$scope,$http);

      $scope.editSubmitForm = function(formData,applicationsData) {
          $http.put('/acl/applications/token/'+token+'/'+application_id,{
              description:formData.description,
              notes:formData.notes,
              port:formData.port
            }).
          success(function(applicationsData) {
              $scope.application = applicationsData.applications;
              status = applicationsData.status;

              if(status == 'ok'){

                $scope.alertType = "success";
                $scope.alertText = "La aplicación  se actualizó de manera exitosa, Aplicación: "+$scope.application[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, Aplicación: "+$scope.application[0].description;
              }
            showAlert($scope);

          })
      }
    }
})
