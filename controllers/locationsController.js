Acl.controller("locationsController", function($scope, locationsModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[10].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    $scope.proyects = {};

    if(section == 'locations' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getLocations('/acl/locations/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/locations/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getLocations(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/locations/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getLocations(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows){
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/locations/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getLocations(urlApp,$scope,$http);
        }

        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/locations/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getLocations(urlApp,$scope,$http);
        }

        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar la locación?")){
            $http.patch('/acl/locations/token/'+token+'/'+id,{status:0}).
            success(function(locationsData) {
                locations = locationsData.locations[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/locations/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getLocations(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La ubicación se desactivó de manera exitosa, Ubicación: "+locations.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar la locación?")){
            $http.patch('/acl/locations/token/'+token+'/'+id,{status:1}).
            success(function(locationsData) {
                locations = locationsData.locations[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/locations/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getLocations(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La ubicación se activó de manera exitosa, Ubicación: "+locations.description;
                showAlert($scope);

            });
          }
        }
    }

    if(section =='locations' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getProjects('/acl/projects/token/'+token+'/filter/projects.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,locationsData) {
            $http.post('/acl/locations/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                cp:formData.cp,
                street:formData.street,
                colony:formData.colony,
                street_address:formData.street_address,
                apartment_number:formData.apartment_number,
                lot:formData.lot,
                block:formData.block,
                project_id:formData.project_id
            }).
            success(function(locationsData) {
                $scope.location = locationsData.locations;
                status = locationsData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "La ubicación se agregó de manera exitosa, Ubicación: "+$scope.location[0].description;
                    formData.description= '';
                    formData.cp="";
                    formData.notes="";
                    formData.street="";
                    formData.colony="";
                    formData.street_address="";
                    formData.apartment_number="";
                    formData.lot="";
                    formData.block="";
                    formData.project_id=[''];
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una descripción repetida";
                }
                showAlert($scope);
            })
        }
    }



    if(section =='locations' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      location_id = $routeParams.location_id;
      urlApp = '/acl/locations/token/'+token+'/'+location_id;

      getLocations(urlApp,$scope,$http);

      getProjects('/acl/projects/token/'+token+'/filter/projects.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,locationsData) {
        $http.put('/acl/locations/token/'+token+'/'+ location_id,{
              description:formData.description,
              notes:formData.notes,
              cp:formData.cp,
              street:formData.street,
              colony:formData.colony,
              street_address:formData.street_address,
              apartment_number:formData.apartment_number,
              lot:formData.lot,
              block:formData.block,
              project_id:formData.project_id
        }).
        success(function(locationsData) {
            $scope.location = locationsData.locations;
            status = locationsData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "La ubicación se actualizo de manera exitosa, Ubicación: "+$scope.location[0].description;
            }
            else{
              $scope.alertType = "danger";
              $scope.alertText = "No es posible ingresar un nombre repetido, Ubicación: "+$scope.location[0].description;

            }
          showAlert($scope);
        })
      }
    }

})
