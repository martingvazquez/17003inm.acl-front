Acl.controller("contractorsController", function($scope, contractorsModel,$http,$location,$routeParams){

  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[4].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    $scope.contractorsTypes = {};

    if(section == 'contractors' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getContractors('/acl/contractors/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/contractors/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getContractors(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/contractors/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getContractors(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows){
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/contractors/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getContractors(urlApp,$scope,$http);
        }

        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/contractors/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getContractors(urlApp,$scope,$http);
        }

        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el contratista?")){
            $http.patch('/acl/contractors/token/'+token+'/'+id,{status:0}).
            success(function(contractorsData) {
                contractors = contractorsData.contractors[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/contractors/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getContractors(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El contratista se desactivó de manera exitosa, Contratista: "+contractors.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el contratista?")){
            $http.patch('/acl/contractors/token/'+token+'/'+id,{status:1}).
            success(function(contractorsData) {
                contractors = contractorsData.contractors[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/contractors/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getContractors(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El contratista se activó de manera exitosa, Contratista: "+contractors.description;
                showAlert($scope);

            });
          }
        }
    }

    if(section =='contractors' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getContractorTypes('/acl/contractor-types/token/'+token+'/filter/contractor_types.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,contractorsData) {
            $http.post('/acl/contractors/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                name:formData.name,
                last_name:formData.last_name,
                second_last_name:formData.second_last_name,
                phone_number:formData.phone_number,
                email:formData.email,
                curp:formData.curp,
                rfc:formData.rfc,
                cp:formData.cp,
                street:formData.street,
                colony:formData.colony,
                street_address:formData.street_address,
                apartment_number:formData.apartment_number,
                business_name:formData.business_name,
                legal_entity:formData.legal_entity,
                contractor_type_id:formData.contractor_type_id
            }).
            success(function(contractorsData) {
                $scope.contractor = contractorsData.contractors;
                status = contractorsData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El contratista se agregó de manera exitosa, Contratista: "+$scope.contractor[0].name;
                    formData.notes="";
                    formData.name="";
                    formData.last_name = '';
                    formData.second_last_name = '';
                    formData.phone_number = '';
                    formData.email = '';
                    formData.curp = '';
                    formData.rfc = '';
                    formData.cp="";
                    formData.notes="";
                    formData.street="";
                    formData.colony="";
                    formData.street_address="";
                    formData.apartment_number="";
                    formData.business_name="";
                    formData.legal_entity="";
                    formData.contractor_type_id=[''];
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una CURP o RFC repetida";
                }
                showAlert($scope);
            })
        }
    }



    if(section =='contractors' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      contractor_id = $routeParams.contractor_id;
      urlApp = '/acl/contractors/token/'+token+'/'+contractor_id;

      getContractors(urlApp,$scope,$http);

      getContractorTypes('/acl/contractor-types/token/'+token+'/filter/contractor_types.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,contractorsData) {
        $http.put('/acl/contractors/token/'+token+'/'+ contractor_id,{
          description:formData.description,
          notes:formData.notes,
          name:formData.name,
          last_name:formData.last_name,
          second_last_name:formData.second_last_name,
          phone_number:formData.phone_number,
          email:formData.email,
          curp:formData.curp,
          rfc:formData.rfc,
          cp:formData.cp,
          street:formData.street,
          colony:formData.colony,
          street_address:formData.street_address,
          apartment_number:formData.apartment_number,
          business_name:formData.business_name,
          legal_entity:formData.legal_entity,
          contractor_type_id:formData.contractor_type_id
        }).
        success(function(contractorsData) {
            $scope.contractor = contractorsData.contractors;
            status = contractorsData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "El contratista se actualizo de manera exitosa, Contratista: "+$scope.contractor[0].name;
            }
            else{
              $scope.alertType = "danger";
              $scope.alertText = "No es posible ingresar una CURP o RFC repetido, Contratista: "+$scope.contractor[0].curp +$scope.contractor[0].rfc;

            }
          showAlert($scope);
        })
      }
    }

})
