Acl.controller("clientsController", function($scope, clientsModel,$http,$location,$routeParams){

    token = getToken();
    if (!token){
      location.href = "/17003inm.acl-front/#/";
    }

    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[2].title);


    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    $scope.clientTypes = {};
    $scope.changeFinancingTypes = function(financing_type_id){
      $scope.selectFinancing = {}
      $scope.selectFinancing.show = true;
      $scope.optionDefault = false;
      getFinancings('/acl/financings/token/'+token+'/filters2/financings.financing_type_id/'+financing_type_id+'/financings.status/1',$scope,$http,true);
      $scope.formData.financing_id=[''];
    }

    if(section == 'clients' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getClients($location, '/acl/clients/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/clients/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getClients($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/clients/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getClients($location,urlApp,$scope,$http);

        }
        $scope.changeRowsPage = function(n_rows){
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=n_rows;
            urlApp = '/acl/clients/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getClients($location,urlApp,$scope,$http);
        }

        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/clients/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getClients($location,urlApp,$scope,$http);
        }

        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el cliente?")){
            $http.patch('/acl/clients/token/'+token+'/'+id,{status:0}).
            success(function(clientsData) {
                clients = clientsData.clients[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/clients/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getClients($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El cliente se desactivó de manera exitosa, Cliente: "+clients.name;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el cliente?")){
            $http.patch('/acl/clients/token/'+token+'/'+id,{status:1}).
            success(function(clientsData) {
                clients = clientsData.clients[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/clients/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getClients($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El cliente se activó de manera exitosa, Cliente: "+clients.name;
                showAlert($scope);

            });
          }
        }
    }

    if(section =='clients' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getClientTypes('/acl/client-types/token/'+token+'/filter/client_types.status/1',$scope,$http,true);
        getFinancings('/acl/financings/token/'+token+'/filter/financing.status/1',$scope,$http,true);
        getFinancingTypes('/acl/financing-types/token/'+token+'/filter/financing_type.status/1',$scope,$http,true);
        getAgents('/acl/agents/token/'+token+'/filter/agents.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,clientsData) {

          var date = document.getElementById("birthdate").value;


            $http.post('/acl/clients/token/'+token,{
                name:formData.name,
                last_name:formData.last_name,
                second_last_name:formData.second_last_name,
                phone_number:formData.phone_number,
                email:formData.email,
                curp:formData.curp,
                rfc:formData.rfc,
                notes:formData.notes,
                gender:formData.gender,
                birth_date:date,
                place_of_birth:formData.place_of_birth,
                purchase_range_initial:formData.purchase_range_initial,
                purchase_range_final:formData.purchase_range_final,
                client_type_id:formData.client_type_id,
                financing_id:formData.financing_id,
                agent_id:formData.agent_id
            }).
            success(function(clientsData) {
                $scope.client = clientsData.clients;
                status = clientsData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El cliente se agregó de manera exitosa, Cliente: "+$scope.client[0].name;
                    formData.name= '';
                    formData.last_name= '';
                    formData.second_last_name= '';
                    formData.phone_number= '';
                    formData.email= '';
                    formData.curp="";
                    formData.rfc= '';
                    formData.notes="";
                    formData.gender="";
                    formData.birth_date= document.getElementById("birthdate").value = "dd/mm/aaaa";
                    formData.place_of_birth="";
                    formData.purchase_range_initial="";
                    formData.purchase_range_final="";
                    formData.client_type_id=[''];
                    formData.financing_id =[''];
                    formData.financig_type_id =[''];
                    formData.agent_id =[''];
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una CURP o RFC repetidas";
                }
                showAlert($scope);
            })
        }
    }



    if(section =='clients' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      client_id = $routeParams.client_id;
      urlApp = '/acl/clients/token/'+token+'/'+client_id;
      getClients($location,urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,clientsData) {
        var date = document.getElementById("birthdate").value;
        formData.birth_date = date;
        $http.put('/acl/clients/token/'+token+'/'+ client_id,{
          name:formData.name,
          last_name:formData.last_name,
          second_last_name:formData.second_last_name,
          phone_number:formData.phone_number,
          email:formData.email,
          curp:formData.curp,
          rfc:formData.rfc,
          notes:formData.notes,
          gender:formData.gender,
          birth_date:formData.birth_date,
          place_of_birth:formData.place_of_birth,
          purchase_range_initial:formData.purchase_range_initial,
          purchase_range_final:formData.purchase_range_final,
          client_type_id:formData.client_type_id,
          financing_id:formData.financing_id,
          agent_id:formData.agent_id
        }).
        success(function(clientsData) {
            $scope.client = clientsData.clients;
            status = clientsData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "El cliente se actualizo de manera exitosa, Cliente: "+$scope.client[0].name;
            }
            else{
              $scope.alertType = "danger";
              $scope.alertText = "No es posible ingresar una CURP o RFC repetido, Cliente: "+$scope.client[0].name;

            }
          showAlert($scope);
        })

      }
    }

    if(section =='clients' && action == 'locations'){
      $scope.actionTitle = FirstUpperCase(act.action[3].title);
    }


})
