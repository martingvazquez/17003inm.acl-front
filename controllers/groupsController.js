Acl.controller("groupsController", function($scope, groupsModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[4].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'groups' && action == 'urls'){
      var group_id = $routeParams.group_id;
      var urlApp = '/acl/groups/token/'+token+'/permissions/'+group_id+'/1'
      getGroupsPermission(urlApp,$scope,$http);
      $scope.formData.url = [];
      $scope.init = function(index,url_id,selected){


        $scope.formData.url[index] = {}
        $scope.formData.url[index].selected = selected;
        $scope.formData.url[index].url_id = url_id;
      }

      $scope.unselect = function(index,url_id,selected){
        alert(selected);
      }

      $scope.submitForm = function() {
        nUrls = $scope.formData.url.length;
        urls_id = [];
        j=0;
        for(i = 0; i<nUrls; i++){
          if($scope.formData.url[i].selected){
            urls_id[j]=$scope.formData.url[i].url_id;
            j++;
          }
        }

        $http.post('/acl/groups_urls',{
            group_id:group_id,
            urls_id:urls_id
          }).
        success(function(groupsData) {
          if(groupsData.status == 'ok'){
            $scope.alertType = "success";
            $scope.alertText = "Los permisos se agregaron de manera exitosa";
          }
          else{
            $scope.alertType = "danger";
            $scope.alertText = "Error al agregar los permisos";

          }
        showAlert($scope);
        })

      }
    }


    if(section == 'groups' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getGroups($location,'/acl/groups/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/groups/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getGroups($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/groups/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getGroups($location,urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/groups/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getGroups($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/groups/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getGroups($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el país?")){
            $http.patch('/acl/groups/token/'+token+'/'+id,{status:0}).
            success(function(groupsData) {
                groups = groupsData.groups[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/groups/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getGroups($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La aplicación se desactivó de manera exitosa, Aplicación: "+groups.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el país?")){
            $http.patch('/acl/groups/token/'+token+'/'+id,{status:1}).
            success(function(groupsData) {
                groups = groupsData.groups[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/groups/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getGroups($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El grupo se activó de manera exitosa, Grupo: "+groups.description;
                showAlert($scope);
            });
          }
        }
    }
    if(section =='groups' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getCompanies($location,'/acl/companies/token/'+token+'/filter/companies.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,groupsData) {
            $http.post('/acl/groups/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                companie_id:formData.companie_id
              }).
            success(function(groupsData) {
              var error = groupsData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
                $scope.group = groupsData;

              $scope.group = groupsData.groups;
              status = groupsData.status;

              if(status == 'ok'){

                $scope.alertType = "success";
                $scope.alertText = "El grupo se agregó de manera exitosa, Grupo: "+$scope.group[0].description;
                formData.description="";
                formData.notes="";
                formData.companie_id="";
              }
              else{
                // $scope.alertType = "danger";
                // $scope.alertText = "No es posible agregar una descripcion repetida";
                error = groupsData.error;
                $scope.alertType = "danger";
                $scope.alertText = groupsData.message;

              }
            showAlert($scope);

            })
        }
    }
    if(section =='groups' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      group_id = $routeParams.group_id;
      urlApp = '/acl/groups/token/'+token+'/'+group_id;
      getGroups($location,urlApp,$scope,$http);
      getCompanies($location,'/acl/companies/token/'+token+'/filter/companies.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,groupsData) {
          $http.put('/acl/groups/token/'+token+'/'+group_id,{
                    description:formData.description,
                    notes:formData.notes,
                    companie_id: formData.companie_id
                  }).
          success(function(groupsData) {
              $scope.group = groupsData.groups;
              status = groupsData.status;

              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El grupo se actualizó de manera exitosa, Grupo: "+$scope.group[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, Grupo: "+$scope.group[0].description;
              }
            showAlert($scope);

          })
      }
    }
})
