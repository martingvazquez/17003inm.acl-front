Acl.controller("contractorTypesController", function($scope, contractorTypesModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }

    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[5].title);

    $scope.typeOrder = 'ASC';
    $scope.filter='';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'contractor-types' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getContractorTypes('/acl/contractor-types/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/contractor-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getContractorTypes(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/contractor-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getContractorTypes(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/contractor-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getContractorTypes(urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/contractor-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getContractorTypes(urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el tipo de contratista?")){
            $http.patch('/acl/contractor-types/token/'+token+'/'+id,{status:0}).
            success(function(contractorTypesData) {
                contractorType = contractorTypesData.contractor_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/contractor-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getContractorTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El contratista se desactivó de manera exitosa, Contratista: "+contractorType.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el contratista?")){
            $http.patch('/acl/contractor-types/token/'+token+'/'+id,{status:1}).
            success(function(contractorTypesData) {
                contractorType = contractorTypesData.contractor_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/contractor-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getContractorTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El contratista se desactivó de manera exitosa, tipo de Contratista: "+contractorType.description;
                showAlert($scope);
            });
          }
        }
    }

    if(section =='contractor-types' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,contractorTypesData) {
            $http.post('/acl/contractor-types/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(contractorTypesData) {
                contractor_type = contractorTypesData.contractor_types[0];
                status = contractorTypesData.status;

                if(status == 'ok'){

                  $scope.alertType = "success";
                  $scope.alertText = "El tipo de contratista se agregó de manera exitosa, Tipo de contratista: " + contractor_type[contractor_type.length - 1].description;
                  formData.description="";
                  formData.notes="";
                }
                else{
                  $scope.alertType = "danger";
                  $scope.alertText = "No es posible agregar un contratista repetida";

                }
              showAlert($scope);
            })
        }
    }

    if(section =='contractor-types' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      contractor_type_id = $routeParams.contractor_type_id;
      urlApp = '/acl/contractor-types/token/'+token+'/'+contractor_type_id;
      getContractorTypes(urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,contractorTypesData) {
          $http.put('/acl/contractor-types/token/'+token+'/'+contractor_type_id,{description:formData.description,notes:formData.notes}).
          success(function(contractorTypesData) {
              $scope.contractorType = contractorTypesData.contractor_types;
              status = contractorTypesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El tipo de contratista se actualizó de manera exitosa, Tipo de Contratista: "+$scope.contractorType[0].description;
              }
              else{
               $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar un contratista repetida, Tipo de Contratista: "+$scope.contractorType[0].description;

              }
            showAlert($scope);
          })
      }
    }

})
