Acl.controller("agentTypesController", function($scope, agentTypesModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[1].title);


    $scope.typeOrder = 'ASC';
    $scope.filter='';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'agent-types' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getAgentTypes('/acl/agent-types/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/agent-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getAgentTypes(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/agent-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getAgentTypes(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/agent-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getAgentTypes(urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/agent-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getAgentTypes(urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el tipo de agente?")){
            $http.patch('/acl/agent-types/token/'+token+'/'+id,{status:0}).
            success(function(agentTypesData) {
                agentType = agentTypesData.agent_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/agent-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getAgentTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El agente se desactivó de manera exitosa, Cliente: "+agentType.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el agente?")){
            $http.patch('/acl/agent-types/token/'+token+'/'+id,{status:1}).
            success(function(agentTypesData) {
                agentType = agentTypesData.agent_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/agent-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getAgentTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El agente se desactivó de manera exitosa, Tipo de agente: "+agentType.description;
                showAlert($scope);
            });
          }
        }
    }

    if(section =='agent-types' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,agentTypesData) {
            $http.post('/acl/agent-types/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(agentTypesData) {
                agent_type = agentTypesData.agent_types[0];
                status = agentTypesData.status;

                if(status == 'ok'){

                  $scope.alertType = "success";
                  $scope.alertText = "El tipo de agente se agregó de manera exitosa, Tipo de agente: "+ agent_type[agent_type.length - 1].description;
                  formData.description="";
                  formData.notes="";
                }
                else{
                  $scope.alertType = "danger";
                  $scope.alertText = "No es posible agregar una descripción repetida";

                }
              showAlert($scope);
            })
        }
    }

    if(section =='agent-types' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      agent_type_id = $routeParams.agent_type_id;
      urlApp = '/acl/agent-types/token/'+token+'/'+agent_type_id;
      getAgentTypes(urlApp,$scope,$http);

      $scope.editSubmitForm = function(formData,agentTypesData) {
          $http.put('/acl/agent-types/token/'+token+'/'+agent_type_id,{description:formData.description,notes:formData.notes}).
          success(function(agentTypesData) {
              $scope.agentType = agentTypesData.agent_types;
              status = agentTypesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El tipo de agente se actualizó de manera exitosa, Tipo de Agente: "+$scope.agentType[0].description;
              }
              else{
               $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripción repetida, Tipo de Agente: "+$scope.agentType[0].description;

              }
            showAlert($scope);
          })
      }
    }

})
