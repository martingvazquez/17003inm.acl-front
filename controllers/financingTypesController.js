Acl.controller("financingTypesController", function($scope, financingTypesModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[9].title);

    $scope.typeOrder = 'ASC';
    $scope.filter='';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'financing-types' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getFinancingTypes('/acl/financing-types/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/financing-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getFinancingTypes(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/financing-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getFinancingTypes(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/financing-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getFinancingTypes(urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/financing-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getFinancingTypes(urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el tipo de financiamiento?")){
            $http.patch('/acl/financing-types/token/'+token+'/'+id,{status:0}).
            success(function(financingTypesData) {
                financingType = financingTypesData.financing_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/financing-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getFinancingTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El tipo de financiamiento se desactivó de manera exitosa, Financiamiento: "+financingType.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el tipo de financiamiento?")){
            $http.patch('/acl/financing-types/token/'+token+'/'+id,{status:1}).
            success(function(financingTypesData) {
                financingType = financingTypesData.financing_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/financing-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getFinancingTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El tipo de financiamiento se desactivó de manera exitosa, Tipo de financiamiento: "+financingType.description;
                showAlert($scope);
            });
          }
        }
    }

    if(section =='financing-types' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,financingTypesData) {
            $http.post('/acl/financing-types/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(financingTypesData) {
                financingType = financingTypesData.financing_types[0];
                status = financingTypesData.status;

                if(status == 'ok'){

                  $scope.alertType = "success";
                  $scope.alertText = "El tipo de financiamiento se agregó de manera exitosa, Tipo de financiamiento: "+ financingType[financingType.length - 1].description;
                  formData.description="";
                  formData.notes="";
                }
                else{
                  $scope.alertType = "danger";
                  $scope.alertText = "No es posible agregar una descripción repetida";

                }
              showAlert($scope);
            })
        }
    }

    if(section =='financing-types' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      financing_type_id = $routeParams.financing_type_id;
      urlApp = '/acl/financing-types/token/'+token+'/'+financing_type_id;
      getFinancingTypes(urlApp,$scope,$http);

      $scope.editSubmitForm = function(formData,financingTypesData) {
          $http.put('/acl/financing-types/token/'+token+'/'+financing_type_id,{description:formData.description,notes:formData.notes}).
          success(function(financingTypesData) {
              $scope.financingType = financingTypesData.financing_types;
              status = financingTypesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El tipo de financiamiento se actualizó de manera exitosa, Tipo de financiamiento: "+ $scope.financingType[0].description;
              }
              else{
               $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripción repetida, Tipo de financiamiento: "+ $scope.financingType[0].description;

              }
            showAlert($scope);
          })
      }
    }

})
