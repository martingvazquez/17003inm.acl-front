Acl.controller("urlsController", function($scope, urlsModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[7].title);

    //variables filtros
    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItemsUrls = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    // variables alertas
    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    //variables tablas foraneas
    $scope.resources = {};

    //funciones Generales

    $scope.changeApplications = function(application_id){
      $scope.selectResource = {}
      $scope.selectResource.show = true;
      $scope.optionDefault = false;
      getResources($location,'/acl/resources/token/'+token+'/filters2/resources.application_id/'+application_id+'/resources.status/1',$scope,$http,true);
      $scope.formData.resource_id=[''];
    }


    if(section == 'urls' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getUrls($location,'/acl/urls/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/urls/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUrls($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/urls/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUrls($location,urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/urls/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUrls($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/urls/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUrls($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar la url?")){
            $http.patch('/acl/urls/'+id,{status:0}).
            success(function(urlsData) {
                urls = urlsData.urls[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/urls/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getUrls($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La url se desactivó de manera exitosa, Url: "+urls.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar la url?")){
            $http.patch('/acl/urls/'+id,{status:1}).
            success(function(urlsData) {
                urls = urlsData.urls[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/urls/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getUrls($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "La url se activó de manera exitosa, Url: "+urls.description;
                showAlert($scope);

            });
          }
        }
    }
    if(section =='urls' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getApplications($location,'/acl/applications/token/'+token+'/filter/applications.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,urlsData){
            $http.post('/acl/urls/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                resource_id:formData.resource_id,
                method:formData.method
            }).
            success(function(urlsData) {
                var error = urlsData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
                $scope.url = urlsData;
                $scope.url = urlsData.urls;
                status = urlsData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "La url se agregó de manera exitosa, Url: "+$scope.url[0].description;
                    formData.description="";
                    formData.notes="";
                    formData.resource_id=[''];
                    formData.application_id=[''];
                    formData.method = '';
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una descripcion repetida";
                }
                showAlert($scope);
            })
        }
    }
    if(section =='urls' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      url_id = $routeParams.url_id;
      urlApp = '/acl/urls/token/'+token+'/'+url_id;
      getUrls($location,urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,urlsData) {
          $http.put('/acl/urls/token/'+token+'/'+url_id,{
              description:formData.description,
              notes:formData.notes,
              resource_id:formData.resource_id,
              method:formData.method
            }).
          success(function(urlsData) {
              $scope.url = urlsData.urls;
              status = urlsData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "La url se actualizó de manera exitosa, Url: "+$scope.url[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, Url: "+$scope.url[0].description;
              }
            showAlert($scope);
          })
      }
    }
})
