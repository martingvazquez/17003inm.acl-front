Acl.controller("phasesController", function($scope, phasesModel,$http,$location,$routeParams){

    token = getToken();
    if (!token){
      location.href = "/17003inm.acl-front/#/";
    }

    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[11].title);

    $scope.typeOrder = 'ASC';
    $scope.filter='';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'phases' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getPhases('/acl/phases/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/phases/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getPhases(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/phases/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getPhases(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/phases/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getPhases(urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/phases/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getPhases(urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el tipo de etapa?")){
            $http.patch('/acl/phases/'+id,{status:0}).
            success(function(phasesData) {
                phase = phasesData.phases[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/phases/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getPhases(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "la etapa se desactivó de manera exitosa, Etapa: "+phase.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el etapa?")){
            $http.patch('/acl/phases/token/'+token+'/'+id,{status:1}).
            success(function(phasesData) {
                phase = phasesData.phases[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/phases/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getPhases(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "la etapa se desactivó de manera exitosa, tipo de Etapa: "+phase.description;
                showAlert($scope);
            });
          }
        }
    }

    if(section =='phases' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,phasesData) {
            $http.post('/acl/phases/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(phasesData) {
                phase = phasesData.phases[0];
                status = phasesData.status;

                if(status == 'ok'){

                  $scope.alertType = "success";
                  $scope.alertText = "La etapa se agregó de manera exitosa, Etapa: " + phase[phase.length - 1].description;
                  formData.description="";
                  formData.notes="";
                }
                else{
                  $scope.alertType = "danger";
                  $scope.alertText = "No es posible agregar una etapa repetida";

                }
              showAlert($scope);
            })
        }
    }

    if(section =='phases' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      phase_id = $routeParams.phase_id;
      urlApp = '/acl/phases/token/'+token+'/'+phase_id;
      getPhases(urlApp,$scope,$http);

      $scope.editSubmitForm = function(formData,phasesData) {
          $http.put('/acl/phases/token/'+token+'/'+ phase_id,{description:formData.description,notes:formData.notes}).
          success(function(phasesData) {
              $scope.phase = phasesData.phases;
              status = phasesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "La etapa se actualizó de manera exitosa, Etapa: "+$scope.phase[0].description;
              }
              else{
               $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una etapa repetida, Etapa: "+$scope.phase[0].description;

              }
            showAlert($scope);
          })
      }
    }

})
