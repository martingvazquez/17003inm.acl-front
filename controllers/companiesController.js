Acl.controller("companiesController", function($scope, companiesModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[2].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    $scope.changeCountries = function(country_id){
      $scope.selectState = {}
      $scope.selectState.show = true;
      $scope.optionDefault = false;
      getStates($location,'/acl/states/token/'+token+'/filters2/states.country_id/'+country_id+'/states.status/1',$scope,$http,true);
      $scope.formData.state_id=[''];
      $scope.selectCity = {}
      $scope.selectCity.show = true;
      $scope.formData.city_id=[''];
    }

    $scope.changeStates = function(state_id){
      $scope.selectCity = {}
      $scope.selectCity.show = true;
      $scope.optionDefault = false;
      getCities($location,'/acl/cities/token/'+token+'/filters2/cities.state_id/'+state_id+'/cities.status/1',$scope,$http,true);
      $scope.formData.city_id=[''];
    }
    if(section == 'companies' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getCompanies($location,'/acl/companies/token/'+token,$scope,$http,true);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/companies/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCompanies($location,urlApp,$scope,$http,true);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/companies/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCompanies($location,urlApp,$scope,$http,true);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/companies/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCompanies($location,urlApp,$scope,$http,true);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/companies/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getCompanies($location,urlApp,$scope,$http,true);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar la empresa?")){
            $http.patch('/acl/companies/token/'+token+'/'+id,{status:0}).
            success(function(companiesData) {
                companies = companiesData.companies[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/companies/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getCompanies($location,urlApp,$scope,$http);
                if(companiesData.error==4){
                  error = companiesData.error;
                  $scope.alertType = "danger";
                  $scope.alertText = companiesData.mensaje;
                }
                else{
                  $scope.alertType = "success";
                  $scope.alertText = "La empresa se desactivó de manera exitosa, Empresa: "+companies.description;

                }
                showAlert($scope);
            });
          }
        }
        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar la empresa?")){
            $http.patch('/acl/companies/token/'+token+'/'+id,{status:1}).
            success(function(companiesData) {
                companies = companiesData.companies[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/companies/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getCompanies($location,urlApp,$scope,$http);
                if(companiesData.error==4){
                  error = companiesData.error;
                  $scope.alertType = "danger";
                  $scope.alertText = companiesData.mensaje;
                }
                else{
                  $scope.alertType = "success";
                  $scope.alertText = "La empresa se activó de manera exitosa, Empresa: "+companies.description;

                }
                showAlert($scope);

            });
          }
        }
    }
    if(section =='companies' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
      getCountries($location,'/acl/countries/token/'+token+'/filter/countries.status/1',$scope,$http,true);
        $scope.submitForm = function(formData,companiesData) {
          $http.post('/acl/companies/token/'+token,{description:formData.description,notes:formData.notes,city_id:formData.city_id}).
            success(function(companiesData) {
              var error = companiesData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
                  $scope.companie = companiesData.companies;
                  status = companiesData.status;
                  if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "La empresa se agregó de manera exitosa, País: "+$scope.companie[0].description;
                    formData.description="";
                    formData.notes="";
                    formData.countrie_id='';
                    formData.state_id='';
                    formData.city_id='';
                  }
                  else{
                    // $scope.alertType = "danger";
                    // $scope.alertText = "No es posible agregar una description repetida.";
                    error = companiesData.error;
                    $scope.alertType = "danger";
                    $scope.alertText = companiesData.message;
                  }
                showAlert($scope);
            })
        }
    }
    if(section =='companies' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      company_id = $routeParams.company_id;
      urlApp = '/acl/companies/token/'+token+'/'+company_id;
      getCompanies($location,urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,companiesData) {
        $http.put('/acl/companies/token/'+token+'/'+company_id,{
                                                    description:formData.description,
                                                    notes:formData.notes,
                                                    city_id:formData.city_id}).
          success(function(companiesData) {
            $scope.companie = companiesData.companies;
            status = companiesData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "La empresa se actualizó de manera exitosa, Empresa: "+$scope.companie[0].description;
            }
            else{
              error = companiesData.error;
              $scope.alertType = "danger";
              $scope.alertText = companiesData.message;
            }
            showAlert($scope);
          })
      }
    }
})
