Acl.controller("usersController", function($scope, usersModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[8].title);

    //variables filtros
    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    // variables alertas
    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    //variables tablas foraneas
    $scope.groups = {};

    if(section == 'users' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getUsers($location,'/acl/users/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/users/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUsers($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/users/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUsers($location,urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/users/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUsers($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/users/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getUsers($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el estado?")){
            $http.patch('/acl/users/token/'+token+'/'+id,{status:0}).
            success(function(usersData) {
                users = usersData.users[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/users/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getUsers($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El estado se desactivó de manera exitosa, Estado: "+users.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el estado?")){
            $http.patch('/acl/users/token/'+token+'/'+id,{status:1}).
            success(function(usersData) {
                users = usersData.users[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/users/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getUsers($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El estado se activó de manera exitosa, Estado: "+users.description;
                showAlert($scope);

            });
          }
        }
    }
    if(section =='users' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getGroups($location,'/acl/groups/token/'+token+'/filter/groups.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,usersData) {
            $http.post('/acl/users/token/'+token,{
                name:formData.name,
                last_name:formData.last_name,
                second_last_name:formData.second_last_name,
                user:formData.user,
                password:formData.password,
                description:formData.description,
                notes:formData.notes,
                group_id:formData.group_id
            }).
            success(function(usersData) {
                var error = usersData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
                $scope.user = usersData;
                $scope.user = usersData.users;
                status = usersData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El usuario se agregó de manera exitosa, Usuario: "+$scope.user[0].description;
                    formData.description="";
                    formData.notes="";
                    formData.group_id='';
                    formData.name = '';
                    formData.last_name='';
                    formData.second_last_name = '';
                    formData.user='';
                    formData.password='';
                }
                else{
                  error = usersData.error;
                  $scope.alertType = "danger";
                  $scope.alertText = usersData.message;
                }
                showAlert($scope);
            })
        }
    }
    if(section =='users' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      user_id = $routeParams.user_id;
      urlApp = '/acl/users/token/'+token+'/'+user_id;
      getUsers($location,urlApp,$scope,$http);
      getGroups($location,'/acl/groups/token/'+token+'/filter/groups.status/1',$scope,$http,true);
      $scope.editSubmitForm = function(formData,usersData) {
          $http.put('/acl/users/token/'+token+'/'+user_id,{
              description:formData.description,
              notes:formData.notes,
              group_id:formData.group_id,
              name:formData.name,
              last_name:formData.last_name,
              second_last_name:formData.second_last_name,
              user:formData.user,
              password:formData.password
            }).
          success(function(usersData) {
              $scope.user = usersData.users;
              status = usersData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El usuario se actualizó de manera exitosa, Usuario: "+$scope.user[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, Usuario: "+$scope.user[0].description;
              }
            showAlert($scope);
          })
      }
    }
})
