Acl.controller("documentsController", function($scope, documentsModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[6].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    $scope.documentsTypes = {};

    if(section == 'documents' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getDocuments('/acl/documents/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/documents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getDocuments(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/documents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getDocuments(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows){
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/documents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getDocuments(urlApp,$scope,$http);
        }

        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/documents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getDocuments(urlApp,$scope,$http);
        }

        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el documento?")){
            $http.patch('/acl/documents/token/'+token+'/'+id,{status:0}).
            success(function(documentsData) {
                documents = documentsData.documents[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/documents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getDocuments(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El documento se desactivó de manera exitosa, Documento: "+documents.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el documento?")){
            $http.patch('/acl/documents/token/'+token+'/'+id,{status:1}).
            success(function(documentsData) {
                documents = documentsData.documents[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/documents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getDocuments(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El documento se activó de manera exitosa, Documento: "+documents.description;
                showAlert($scope);

            });
          }
        }
    }

    if(section =='documents' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getDocumentTypes('/acl/document-types/token/'+token+'/filter/document_types.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,documentsData) {
            $http.post('/acl/documents/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                file:formData.file,
                document_type_id:formData.document_type_id
            }).
            success(function(documentsData) {
                $scope.document = documentsData.documents;
                status = documentsData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El documento se agregó de manera exitosa, Documento: "+$scope.document[0].description;
                    formData.description= '';
                    formData.notes="";
                    formData.file="";
                    formData.document_type_id=[''];
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una descripción repetida";
                }
                showAlert($scope);
            })
        }
    }



    if(section =='documents' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      document_id = $routeParams.document_id;
      urlApp = '/acl/documents/token/'+token+'/'+document_id;

      getDocuments(urlApp,$scope,$http);

      getDocumentTypes('/acl/document-types/token/'+token+'/filter/document_types.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,documentsData) {
        $http.put('/acl/documents/token/'+token+'/'+ document_id,{
          description:formData.description,
          notes:formData.notes,
          file:formData.file,
          document_type_id:formData.document_type_id
        }).
        success(function(documentsData) {
            $scope.document = documentsData.documents;
            status = documentsData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "El documento se actualizo de manera exitosa, Documento: "+$scope.document[0].description;
            }
            else{
              $scope.alertType = "danger";
              $scope.alertText = "No es posible ingresar un nombre repetido, Documento: "+$scope.document[0].description;

            }
          showAlert($scope);
        })
      }
    }

})
