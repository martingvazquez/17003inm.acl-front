Acl.controller("clientTypesController", function($scope, clientTypesModel,$http,$location,$routeParams){

  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[3].title);

    $scope.typeOrder = 'ASC';
    $scope.filter='';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'client-types' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getClientTypes('/acl/client-types/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/client-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getClientTypes(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/client-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getClientTypes(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/client-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getClientTypes(urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/client-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getClientTypes(urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el tipo de cliente?")){
            $http.patch('/acl/client-types/token/'+token+'/'+id,{status:0}).
            success(function(clientTypesData) {
                clientType = clientTypesData.client_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/client-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getClientTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El cliente se desactivó de manera exitosa, Cliente: "+clientType.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el cliente?")){
            $http.patch('/acl/client-types/token/'+token+'/'+id,{status:1}).
            success(function(clientTypesData) {
                clientType = clientTypesData.client_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/client-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getClientTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El cliente se desactivó de manera exitosa, tipo de Cliente: "+clientType.description;
                showAlert($scope);
            });
          }
        }
    }

    if(section =='client-types' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,clientTypesData) {
            $http.post('/acl/client-types/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(clientTypesData) {
                clientType = clientTypesData.client_types[0];
                status = clientTypesData.status;

                if(status == 'ok'){

                  $scope.alertType = "success";
                  $scope.alertText = "El tipo de cliente se agregó de manera exitosa, Tipo de cliente: "+ clientType[clientType.length - 1].description;
                  formData.description="";
                  formData.notes="";
                }
                else{
                  $scope.alertType = "danger";
                  $scope.alertText = "No es posible agregar una descripción repetida";

                }
              showAlert($scope);
            })
        }
    }

    if(section =='client-types' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      client_type_id = $routeParams.client_type_id;
      urlApp = '/acl/client-types/token/'+token+'/'+client_type_id;
      getClientTypes(urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,clientTypesData) {
          $http.put('/acl/client-types/token/'+token+'/'+client_type_id,{description:formData.description,notes:formData.notes}).
          success(function(clientTypesData) {
              $scope.clientType = clientTypesData.client_types;
              status = clientTypesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El tipo de cliente se actualizó de manera exitosa, Tipo de Cliente: "+$scope.clientType[0].description;
              }
              else{
               $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripción repetida, Tipo de Cliente: "+$scope.clientType[0].description;

              }
            showAlert($scope);
          })
      }
    }

})
