Acl.controller("agentsController", function($scope, agentsModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }

    var url = $location.url();
    urlSplit=splitUrl(url);

    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[0].title);



    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    $scope.agentTypes = {};

    if(section == 'agents' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getAgents('/acl/agents/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/agents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getAgents(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/agents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getAgents(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows){
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/agents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getAgents(urlApp,$scope,$http);
        }

        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/agents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getAgents(urlApp,$scope,$http);
        }

        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el agente?")){
            $http.patch('/acl/agents/token/'+token+'/'+id,{status:0}).
            success(function(agentsData) {
                agents = agentsData.agents[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/agents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getAgents(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El agente se desactivó de manera exitosa, Agente: "+agents.name;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el agente?")){
            $http.patch('/acl/agents/token/'+token+'/'+id,{status:1}).
            success(function(agentsData) {
                agents = agentsData.agents[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/agents/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getAgents(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El agente se activó de manera exitosa, Agente: "+agents.name;
                showAlert($scope);

            });
          }
        }
    }

    if(section =='agents' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getAgentTypes('/acl/agent-types/token/'+token+'/filter/agent_types.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,agentsData) {
            $http.post('/acl/agents/token/'+token,{
                name:formData.name,
                last_name:formData.last_name,
                second_last_name:formData.second_last_name,
                phone_number:formData.phone_number,
                email:formData.email,
                curp:formData.curp,
                rfc:formData.rfc,
                notes:formData.notes,
                agent_type_id:formData.agent_type_id
            }).
            success(function(agentsData) {
                $scope.agent = agentsData.agents;
                status = agentsData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El agente se agregó de manera exitosa, Agente: "+$scope.agent[0].name;
                    formData.name= '';
                    formData.last_name= '';
                    formData.second_last_name= '';
                    formData.phone_number= '';
                    formData.email= '';
                    formData.curp="";
                    formData.rfc= '';
                    formData.notes="";
                    formData.agent_type_id=[''];
                    formData.financing_id =[''];
                    formData.financig_type_id =[''];
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una CURP o RFC repetidas";
                }
                showAlert($scope);
            })
        }
    }



    if(section =='agents' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      agent_id = $routeParams.agent_id;
      urlApp = '/acl/agents/token/'+token+'/'+agent_id;

      getAgents(urlApp,$scope,$http);

      getAgentTypes('/acl/agent-types/token/'+token+'/filter/agent_types.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,agentsData) {
        $http.put('/acl/agents/'+ agent_id,{
          name:formData.name,
          last_name:formData.last_name,
          second_last_name:formData.second_last_name,
          phone_number:formData.phone_number,
          email:formData.email,
          curp:formData.curp,
          rfc:formData.rfc,
          notes:formData.notes,
          agent_type_id:formData.agent_type_id,
          financing_id:formData.financing_id
        }).
        success(function(agentsData) {
            $scope.agent = agentsData.agents;
            status = agentsData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "El agente se actualizo de manera exitosa, Agente: "+$scope.agent[0].name;
            }
            else{
              $scope.alertType = "danger";
              $scope.alertText = "No es posible ingresar un nombre repetido, Agente: "+$scope.agent[0].name;

            }
          showAlert($scope);
        })
      }
    }

})
