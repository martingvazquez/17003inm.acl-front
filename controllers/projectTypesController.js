Acl.controller("projectTypesController", function($scope, projectTypesModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[13].title);

    $scope.typeOrder = 'ASC';
    $scope.filter='';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'project-types' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getProjectTypes('/acl/project-types/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/project-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getProjectTypes(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/project-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getProjectTypes(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/project-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getProjectTypes(urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/project-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getProjectTypes(urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el tipo de Projecto?")){
            $http.patch('/acl/project-types/token/'+token+'/'+id,{status:0}).
            success(function(ProjectTypesData) {
                ProjectType = ProjectTypesData.Project_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/project-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getProjectTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El Projecto se desactivó de manera exitosa, Projecto: "+ProjectType.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el Projecto?")){
            $http.patch('/acl/project-types/token/'+token+'/'+id,{status:1}).
            success(function(ProjectTypesData) {
                ProjectType = ProjectTypesData.Project_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/project-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getProjectTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El Projecto se desactivó de manera exitosa, tipo de Projecto: "+ProjectType.description;
                showAlert($scope);
            });
          }
        }
    }

    if(section =='project-types' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,ProjectTypesData) {
            $http.post('/acl/project-types/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(ProjectTypesData) {
                project_type = ProjectTypesData.project_types[0];
                status = ProjectTypesData.status;

                if(status == 'ok'){

                  $scope.alertType = "success";
                  $scope.alertText = "El tipo de Proyecto se agregó de manera exitosa, Tipo de Proyecto: " + project_type.description;
                  formData.description="";
                  formData.notes="";
                }
                else{
                  $scope.alertType = "danger";
                  $scope.alertText = "No es posible agregar un Proyecto repetida";

                }
              showAlert($scope);
            })
        }
    }

    if(section =='project-types' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      project_type_id = $routeParams.project_type_id;
      urlApp = '/acl/project-types/token/'+token+'/'+project_type_id;
      getProjectTypes(urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,projectTypesData) {
          $http.put('/acl/project-types/token/'+token+'/'+project_type_id,{description:formData.description,notes:formData.notes}).
          success(function(projectTypesData) {
              $scope.projectType = projectTypesData.project_types;
              status = projectTypesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El tipo de Proyecto se actualizó de manera exitosa, Tipo de Proyecto: "+$scope.projectType[0].description;
              }
              else{
               $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar un Proyecto repetida, Tipo de Proyecto: "+$scope.projectType[0].description;

              }
            showAlert($scope);
          })
      }
    }

})
