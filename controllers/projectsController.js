Acl.controller("projectsController", function($scope, projectsModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[12].title);

    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    $scope.projectsTypes = {};

    $scope.changeContractorTypes = function(contractor_type_id){
      $scope.selectContractor = {}
      $scope.selectContractor.show = true;
      $scope.optionDefault = false;
      getContractors('/acl/contractors/token/'+token+'/filters2/contractors.contractor_type_id/'+contractor_type_id+'/contractor.status/1',$scope,$http,true);
      $scope.formData.contractor_id=[''];
    }


    if(section == 'projects' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getProjects('/acl/projects/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/projects/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getProjects(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/projects/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getProjects(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows){
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/projects/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getProjects(urlApp,$scope,$http);
        }

        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/projects/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;

            getProjects(urlApp,$scope,$http);
        }

        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el proyecto?")){
            $http.patch('/acl/projects/token/'+token+'/'+id,{status:0}).
            success(function(projectsData) {
                projects = projectsData.projects[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/projects/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getProjects(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El proyecto se desactivó de manera exitosa, Proyecto: "+projects.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el proyecto?")){
            $http.patch('/acl/projects/token/'+token+'/'+id,{status:1}).
            success(function(projectsData) {
                projects = projectsData.projects[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/projects/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getProjects(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El proyecto se activó de manera exitosa, Proyecto: "+projects.description;
                showAlert($scope);

            });
          }
        }
    }

    if(section =='projects' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getProjectTypes('/acl/project-types/token/'+token+'/filter/proyect_types.status/1',$scope,$http,true);
        getContractors('/acl/contractors/token/'+token+'/filter/contractor.status/1',$scope,$http,true);
        getContractorTypes('/acl/contractor-types/token/'+token+'/filter/contractor_type.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,projectsData) {
            $http.post('/acl/projects/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                cp:formData.cp,
                street:formData.street,
                colony:formData.colony,
                street_address:formData.street_address,
                apartment_number:formData.apartment_number,
                project_type_id:formData.project_type_id,
                contractor_id:formData.contractor_id
            }).
            success(function(projectsData) {
                $scope.project = projectsData.projects;
                status = projectsData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El proyecto se agregó de manera exitosa, Proyecto: "+$scope.project[0].description;
                    formData.description= '';
                    formData.cp="";
                    formData.notes="";
                    formData.street="";
                    formData.colony="";
                    formData.street_address="";
                    formData.apartment_number="";
                    formData.project_type_id=[''];
                    formData.contractor_id=[''];
                    formData.contractor_type_id=[''];
                }
                else{
                    $scope.alertType = "danger";
                    $scope.alertText = "No es posible agregar una descripción repetida";
                }
                showAlert($scope);
            })
        }
    }



    if(section =='projects' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      project_id = $routeParams.project_id;
      urlApp = '/acl/projects/token/'+token+'/'+project_id;

      getProjects(urlApp,$scope,$http);

      $scope.editSubmitForm = function(formData,projectsData) {
        $http.put('/acl/projects/token/'+token+'/'+ project_id,{
              description:formData.description,
              notes:formData.notes,
              cp:formData.cp,
              street:formData.street,
              colony:formData.colony,
              street_address:formData.street_address,
              apartment_number:formData.apartment_number,
              project_type_id:formData.project_type_id,
              contractor_id:formData.contractor_id
        }).
        success(function(projectsData) {
            $scope.project = projectsData.projects;
            status = projectsData.status;
            if(status == 'ok'){
              $scope.alertType = "success";
              $scope.alertText = "El proyecto se actualizo de manera exitosa, Proyecto: "+$scope.project[0].description;
            }
            else{
              $scope.alertType = "danger";
              $scope.alertText = "No es posible ingresar un nombre repetido, Proyecto: "+$scope.project[0].description;

            }
          showAlert($scope);
        })
      }
    }

})
