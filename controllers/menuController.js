Acl.controller("menuController", function($scope,$rootScope,$location){
  var url = $location.url();
  urlSplit=splitUrl(url);

  app = urlSplit[1];
  section = urlSplit[2];
  action = urlSplit[3];

  token = getToken();
  if(!token){
      $rootScope.token = false;
      $location.path("/");
      document.getElementById("menu-toggle").style.display = "none";
  }  else {
    $rootScope.token = true;
    document.getElementById("menu-toggle").style.display = "flex";
  }

  if(app == 'acl'){
    $("#inm-adm").addClass("not-visible");
  }

  if(app == 'adm'){
    $("#inm-acl").addClass("not-visible");
  }


})
