Acl.controller("statesController", function($scope, statesModel,$http,$location,$routeParams){
    token = getToken();
    if (!token){
      $location.path("/");
    }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    $scope.sectionSubMenu = section;

    var sec = JSON.parse(sections);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[6].title);

    //variables filtros
    $scope.typeOrder = 'ASC';
    $scope.filter = '';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    // variables alertas
    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";

    //variables tablas foraneas
    $scope.countries = {};

    if(section == 'states' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getStates($location,'/acl/states/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/states/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getStates($location,urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/states/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getStates($location,urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/states/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getStates($location,urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/states/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getStates($location,urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el estado?")){
            $http.patch('/acl/states/token/'+token+'/'+id,{status:0}).
            success(function(statesData) {
                states = statesData.states[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/states/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getStates($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El estado se desactivó de manera exitosa, Estado: "+states.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el estado?")){
            $http.patch('/acl/states/token/'+token+'/'+id,{status:1}).
            success(function(statesData) {
                states = statesData.states[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/states/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getStates($location,urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El estado se activó de manera exitosa, Estado: "+states.description;
                showAlert($scope);

            });
          }
        }
    }
    if(section =='states' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        getCountries($location,'/acl/countries/token/'+token+'/filter/countries.status/1',$scope,$http,true);
        $scope.addSubmitForm = function(formData,statesData) {
            $http.post('/acl/states/token/'+token,{
                description:formData.description,
                notes:formData.notes,
                country_id:formData.country_id
            }).
            success(function(statesData) {
                var error = statesData.error;
                if(error == 4){
                    $location.path('/login/denegado');
                }
                $scope.state = statesData;
                $scope.state = statesData.states;
                status = statesData.status;
                if(status == 'ok'){
                    $scope.alertType = "success";
                    $scope.alertText = "El estado se agregó de manera exitosa, Estado: "+$scope.state[0].description;
                    formData.description="";
                    formData.notes="";
                    formData.country_id='';
                }
                else{
                  error = statesData.error;
                  $scope.alertType = "danger";
                  $scope.alertText = statesData.mensaje;
                }
                showAlert($scope);
            })
        }
    }
    if(section =='states' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      state_id = $routeParams.state_id;
      urlApp = '/acl/states/token/'+token+'/'+state_id;
      getStates($location,urlApp,$scope,$http);
      getCountries($location,'/acl/countries/token/'+token+'/filter/countries.status/1',$scope,$http,true);

      $scope.editSubmitForm = function(formData,statesData) {
          $http.put('/acl/states/token/'+token+'/'+state_id,{
              description:formData.description,
              notes:formData.notes,
              country_id:formData.country_id
            }).
          success(function(statesData) {
              $scope.state = statesData.states;
              status = statesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El estado se actualizó de manera exitosa, Estado: "+$scope.state[0].description;
              }
              else{
                $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar una descripcion repetida, Estado: "+$scope.state[0].description;
              }
            showAlert($scope);
          })
      }
    }
})
