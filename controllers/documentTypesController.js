Acl.controller("documentTypesController", function($scope, documentTypesModel,$http,$location,$routeParams){
  token = getToken();
  if (!token){
    location.href = "/17003inm.acl-front/#/";
  }
    var url = $location.url();
    urlSplit=splitUrl(url);
    section = urlSplit[2];
    action = urlSplit[3];

    var sec = JSON.parse(sections_adm);
    var act = JSON.parse(actions);

    $scope.section = section;
    var sectionSubMenu = app+"/"+section;
    $scope.sectionSubMenu = sectionSubMenu;

    $scope.sectionTitle = FirstUpperCase(sec.section[7].title);

    $scope.typeOrder = 'ASC';
    $scope.filter='';
    $scope.column='';
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsPerPage=5;
    $scope.maxSize=5;
    $scope.formData = {};

    $scope.alertMessage = null;
    $scope.alertType = "success";
    $scope.secondsDelay = 4;
    $scope.closable = true;
    $scope.alertText = "";


    if(section == 'document-types' && action == 'list'){
      $scope.actionTitle = FirstUpperCase(act.action[0].title);
        getDocumentTypes('/acl/document-types/token/'+token,$scope,$http);
        $scope.orderByMe = function(column) {
            typeOrder = updateTypeOrder($scope);
            filter = asignFilter($scope);
            $scope.column = column;
            urlApp = '/acl/document-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getDocumentTypes(urlApp,$scope,$http);
        }
        $scope.filterByMe = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/document-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getDocumentTypes(urlApp,$scope,$http);
        }
        $scope.changeRowsPage = function(nRows) {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            $scope.itemsPerPage=nRows;
            urlApp = '/acl/document-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getDocumentTypes(urlApp,$scope,$http);
        }
        $scope.pageChanged = function() {
            column = asignColumn($scope);
            filter = asignFilter($scope);
            urlApp = '/acl/document-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
            getDocumentTypes(urlApp,$scope,$http);
        }
        $scope.deactivate = function(id) {
          if(confirm("¿Estas seguro de desactivar el tipo de documento?")){
            $http.patch('/acl/document-types/token/'+token+'/'+id,{status:0}).
            success(function(documentTypesData) {
                documentType = documentTypesData.document_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/document-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getDocumentTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El documento se desactivó de manera exitosa, Cliente: "+documentType.description;
                showAlert($scope);
            });
          }
        }

        $scope.activate = function(id) {
          if(confirm("¿Estas seguro de activar el documento?")){
            $http.patch('/acl/document-types/token/'+token+'/'+id,{status:1}).
            success(function(documentTypesData) {
                documentType = documentTypesData.document_types[0];
                column = asignColumn($scope);
                filter = asignFilter($scope);
                urlApp = '/acl/document-types/token/'+token+'/'+column+'/'+$scope.typeOrder+'/'+filter+'/'+$scope.currentPage+'/'+$scope.itemsPerPage;
                getDocumentTypes(urlApp,$scope,$http);
                $scope.alertType = "success";
                $scope.alertText = "El documento se desactivó de manera exitosa, tipo de Cliente: "+documentType.description;
                showAlert($scope);
            });
          }
        }
    }

    if(section =='document-types' && action == 'add'){
      $scope.actionTitle = FirstUpperCase(act.action[1].title);
        $scope.addSubmitForm = function(formData,documentTypesData) {
            $http.post('/acl/document-types/token/'+token,{description:formData.description,notes:formData.notes}).
            success(function(documentTypesData) {
                document_type = documentTypesData.document_types[0];
                status = documentTypesData.status;

                if(status == 'ok'){

                  $scope.alertType = "success";
                  $scope.alertText = "El tipo de documento se agregó de manera exitosa, Tipo de documento: " + document_type[document_type.length - 1].description;
                  formData.description="";
                  formData.notes="";
                }
                else{
                  $scope.alertType = "danger";
                  $scope.alertText = "No es posible agregar un documento repetida";

                }
              showAlert($scope);
            })
        }
    }

    if(section =='document-types' && action == 'edit'){
      $scope.actionTitle = FirstUpperCase(act.action[2].title);
      document_type_id = $routeParams.document_type_id;
      urlApp = '/acl/document-types/token/'+token+'/'+document_type_id;
      getDocumentTypes(urlApp,$scope,$http);
      $scope.editSubmitForm = function(formData,documentTypesData) {
          $http.put('/acl/document-types/token/'+token+'/'+document_type_id,{description:formData.description,notes:formData.notes}).
          success(function(documentTypesData) {
              $scope.documentType = documentTypesData.document_types;
              status = documentTypesData.status;
              if(status == 'ok'){
                $scope.alertType = "success";
                $scope.alertText = "El tipo de documento se actualizó de manera exitosa, Tipo de Cliente: "+$scope.documentType[0].description;
              }
              else{
               $scope.alertType = "danger";
                $scope.alertText = "No es posible ingresar un documento repetida, Tipo de Cliente: "+$scope.documentType[0].description;

              }
            showAlert($scope);
          })
      }
    }

})
