// spec.js
describe('Protractor Demo App', function() {
  var user = element(by.model('formData.user'));
  var password = element(by.model('formData.password'));
  var goButton = element(by.model('btnAdd'));

  beforeEach(function() {
    browser.get('http://localhost/16001cuc.ppl-acl.front/#/');
  });

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('CUCIENEGA');
  });
  it('should login with user and password', function() {
    user.sendKeys('root');
    password.sendKeys('moro58');
    goButton.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost/16001cuc.ppl-acl.front/#/login/bienvenido');
  });
  it('should logout', function() {
  	var btnLogout = element(by.id('login'));
    btnLogout.click();
    expect(browser.getCurrentUrl()).toEqual('http://localhost/16001cuc.ppl-acl.front/#/');
  });
});
