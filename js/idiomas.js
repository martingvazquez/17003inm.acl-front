//   idiomas
//   es = español
//   en = inlges

var seleccionaIdioma = 'es';
var sections;
var actions;


switch(seleccionaIdioma) {
    case 'es':
      sections = '{ "section" : [' +
      '{ "title":"aplicaciones" },' +   // 0
      '{ "title":"ciudades" },' +       // 1
      '{ "title":"empresas" },' +      // 2
      '{ "title":"países" },' +         // 3
      '{ "title":"grupos" },' +         // 4
      '{ "title":"recursos" },' +       // 5
      '{ "title":"estados" },' +        // 6
      '{ "title":"urls" },' +           // 7
      '{ "title":"usuarios" },' +           // 8
      '{ "title":"inicio de sesión" } ]}';      // 9

      sections_adm = '{ "section" : [' +
      '{ "title":"agentes" },' +                    // 0
      '{ "title":"tipos de agentes" },' +           // 1
      '{ "title":"clientes" },' +                   // 2
      '{ "title":"tipos de clientes" },' +          // 3
      '{ "title":"contratistas" },' +               // 4
      '{ "title":"tipos de contratistas" },' +      // 5
      '{ "title":"documentos" },' +                 // 6
      '{ "title":"tipos de documentos" },' +        // 7
      '{ "title":"financiamientos" },' +            // 8
      '{ "title":"tipos de financiamientos" },' +   // 9
      '{ "title":"ubicaciones" },' +                // 10
      '{ "title":"etapas" },' +                     // 11
      '{ "title":"proyectos" },' +                  // 12
      '{ "title":"tipos de proyectos" },' +         // 13
      '{ "title":"login" } ]}';                     // 14

      actions = '{ "action" : [' +
      '{ "title":"lista" },' +              // 0
      '{ "title":"agregar" },' +            // 1
      '{ "title":"editar" } ]}';            // 2
      break;
    case 'en':
      sections = '{ "section" : [' +
      '{ "title":"applications" },' +   // 0
      '{ "title":"cities" },' +         // 1
      '{ "title":"companies" },' +      // 2
      '{ "title":"countries" },' +      // 3
      '{ "title":"groups" },' +         // 4
      '{ "title":"resources" },' +      // 5
      '{ "title":"states" },' +         // 6
      '{ "title":"urls" },' +           // 7
      '{ "title":"users" },' +           // 8
      '{ "title":"login" } ]}';         // 9

      sections = '{ "section" : [' +
      '{ "title":"agents" },' +               // 0
      '{ "title":"agents types" },' +         // 1
      '{ "title":"clients" },' +              // 2
      '{ "title":"client types" },' +         // 3
      '{ "title":"contractors" },' +          // 4
      '{ "title":"contractor types" },' +     // 5
      '{ "title":"documents" },' +            // 6
      '{ "title":"documents types" },' +      // 7
      '{ "title":"financings" },' +           // 8
      '{ "title":"financing types" },' +      // 9
      '{ "title":"locations" },' +            // 10
      '{ "title":"phases" },' +               // 11
      '{ "title":"projects" },' +             // 12
      '{ "title":"project types" },' +        // 13
      '{ "title":"menu" } ]}';                // 14

      actions = '{ "action" : [' +
      '{ "title":"list" },' +         // 0
      '{ "title":"add" },' +          // 1
      '{ "title":"edit" } ]}';      // 2
      break;
}
