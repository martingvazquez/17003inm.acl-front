var configAcl = function($routeProvider){
    $routeProvider.when("/", {
        controller: "loginController",
        templateUrl: "views/login/init.html"
    }).when("/login/denegado", {
        controller: "loginController",
        templateUrl: "views/login/denegado.html"
    }).when("/login/bienvenido", {
        controller: "loginController",
        templateUrl: "views/login/bienvenido.html"
    }).when("/login/salir", {
        controller: "loginController",
        templateUrl: "views/login/salir.html"
    }).when("/login/aplicaciones", {
        controller: "loginController",
        templateUrl: "views/login/aplicaciones.html"
    }).when("/acl/login/menu-acl", {
        controller: "loginController",
        templateUrl: "views/login/menu-acl.html"
    }).when("/acl/companies", {
    }).when("/adm/login/menu-adm", {
        controller: "loginController",
        templateUrl: "views/login/menu-adm.html"
    }).when("/acl/companies", {
        controller: "companiesController",
        templateUrl: "views/acl/companies/list.html"
    }).when("/acl/companies/list", {
        controller : "companiesController",
        templateUrl: "views/acl/companies/list.html"
    }).when("/acl/companies/add", {
        controller : "companiesController",
        templateUrl: "views/acl/companies/add.html"
    }).when("/acl/countries/add", {
        controller : "countriesController",
        templateUrl: "views/acl/countries/add.html"
    }).when("/acl/countries/list", {
        controller : "countriesController",
        templateUrl: "views/acl/countries/list.html"
    }).when("/acl/countries", {
        controller : "countriesController",
        templateUrl: "views/acl/countries/list.html"
    }).when("/acl/countries/edit/:country_id", {
        controller : "countriesController",
        templateUrl: "views/acl/countries/edit.html"
    }).when("/acl/countries/cities/:state_id", {
        controller : "countriesController",
        templateUrl: "views/acl/countries/cities.html"
    }).when("/acl/countries/states/:country_id", {
        controller : "countriesController",
        templateUrl: "views/acl/countries/states.html"
    }).when("/acl/companies/edit/:company_id", {
        controller : "companiesController",
        templateUrl: "views/acl/companies/edit.html"
    }).when("/acl/states", {
        controller : "statesController",
        templateUrl: "views/acl/states/list.html"
    }).when("/acl/states/list", {
        controller : "statesController",
        templateUrl: "views/acl/states/list.html"
    }).when("/acl/states/edit/:state_id", {
        controller : "statesController",
        templateUrl: "views/acl/states/edit.html"
    }).when("/acl/states/add", {
        controller : "statesController",
        templateUrl: "views/acl/states/add.html"
    }).when("/acl/cities", {
        controller : "citiesController",
        templateUrl: "views/acl/cities/list.html"
    }).when("/acl/cities/list", {
        controller : "citiesController",
        templateUrl: "views/acl/cities/list.html"
    }).when("/acl/cities/edit/:city_id", {
        controller : "citiesController",
        templateUrl: "views/acl/cities/edit.html"
    }).when("/acl/cities/add", {
        controller : "citiesController",
        templateUrl: "views/acl/cities/add.html"
    }).when("/acl/applications", {
        controller : "applicationsController",
        templateUrl: "views/acl/applications/list.html"
    }).when("/acl/applications/list", {
        controller : "applicationsController",
        templateUrl: "views/acl/applications/list.html"
    }).when("/acl/applications/edit/:application_id", {
        controller : "applicationsController",
        templateUrl: "views/acl/applications/edit.html"
    }).when("/acl/applications/add", {
        controller : "applicationsController",
        templateUrl: "views/acl/applications/add.html"
    }).when("/acl/groups", {
        controller : "groupsController",
        templateUrl: "views/acl/groups/list.html"
    }).when("/acl/groups/list", {
        controller : "groupsController",
        templateUrl: "views/acl/groups/list.html"
    }).when("/acl/groups/edit/:group_id", {
        controller : "groupsController",
        templateUrl: "views/acl/groups/edit.html"
    }).when("/acl/groups/add", {
        controller : "groupsController",
        templateUrl: "views/acl/groups/add.html"
    }).when("/acl/groups/urls/:group_id", {
        controller : "groupsController",
        templateUrl: "views/acl/groups/urls.html"
    }).when("/acl/resources", {
        controller : "resourcesController",
        templateUrl: "views/acl/resources/list.html"
    }).when("/acl/resources/list", {
        controller : "resourcesController",
        templateUrl: "views/acl/resources/list.html"
    }).when("/acl/resources/edit/:resource_id", {
        controller : "resourcesController",
        templateUrl: "views/acl/resources/edit.html"
    }).when("/acl/resources/add", {
        controller : "resourcesController",
        templateUrl: "views/acl/resources/add.html"
    }).when("/acl/urls", {
        controller : "urlsController",
        templateUrl: "views/acl/urls/list.html"
    }).when("/acl/urls/list", {
        controller : "urlsController",
        templateUrl: "views/acl/urls/list.html"
    }).when("/acl/urls/edit/:url_id", {
        controller : "urlsController",
        templateUrl: "views/acl/urls/edit.html"
    }).when("/acl/urls/add", {
        controller : "urlsController",
        templateUrl: "views/acl/urls/add.html"
    }).when("/acl/users", {
        controller : "usersController",
        templateUrl: "views/acl/users/list.html"
    }).when("/acl/users/list", {
        controller : "usersController",
        templateUrl: "views/acl/users/list.html"
    }).when("/acl/users/edit/:user_id", {
        controller : "usersController",
        templateUrl: "views/acl/users/edit.html"
    }).when("/acl/users/add", {
        controller : "usersController",
        templateUrl: "views/acl/users/add.html"
    });

    // CLIENTES

    $routeProvider.when("/adm/clients", {
        controller: "clientsController",
        templateUrl: "views/adm/clients/list.html"
    }).when("/adm/clients/list", {
        controller : "clientsController",
        templateUrl: "views/adm/clients/list.html"
    }).when("/adm/clients/add", {
        controller : "clientsController",
        templateUrl: "views/adm/clients/add.html"
    }).when("/adm/clients/locations/:client_id", {
        controller : "clientsController",
        templateUrl: "views/adm/clients/locations.html"
    }).when("/adm/clients/phases/:client_id", {
        controller : "clientsController",
        templateUrl: "views/adm/clients/phases.html"
    }).when("/adm/client-types/add", {                   // clientTypes
        controller : "clientTypesController",
        templateUrl: "views/adm/clientTypes/add.html"
    }).when("/adm/client-types/list", {
        controller : "clientTypesController",
        templateUrl: "views/adm/clientTypes/list.html"
    }).when("/adm/client-types", {
        controller : "clientTypesController",
        templateUrl: "views/adm/clientTypes/list.html"
    }).when("/adm/client-types/edit/:client_type_id", {
        controller : "clientTypesController",
        templateUrl: "views/adm/clientTypes/edit.html"
    }).when("/adm/clients/edit/:client_id", {           //client_id
        controller : "clientsController",
        templateUrl: "views/adm/clients/edit.html"
    }).when("/adm/financings/add", {                   // financings
        controller : "financingsController",
        templateUrl: "views/adm/financings/add.html"
    }).when("/adm/financings/list", {
        controller : "financingsController",
        templateUrl: "views/adm/financings/list.html"
    }).when("/adm/financings", {
        controller : "financingsController",
        templateUrl: "views/adm/financings/list.html"
    }).when("/adm/financings/edit/:financing_id", {
        controller : "financingsController",
        templateUrl: "views/adm/financings/edit.html"
    }).when("/adm/financing-types/add", {                   // financingTypes
        controller : "financingTypesController",
        templateUrl: "views/adm/financingTypes/add.html"
    }).when("/adm/financing-types/list", {
        controller : "financingTypesController",
        templateUrl: "views/adm/financingTypes/list.html"
    }).when("/adm/financing-types", {
        controller : "financingTypesController",
        templateUrl: "views/adm/financingTypes/list.html"
    }).when("/adm/financing-types/edit/:financing_type_id", {
        controller : "financingTypesController",
        templateUrl: "views/adm/financingTypes/edit.html"
    }).when("/adm/financing-types/phases/:financing_type_id", {
        controller : "financingTypesController",
        templateUrl: "views/adm/financingTypes/phases.html"
    }).when("/adm/phases/add", {                              // phases
        controller : "phasesController",
        templateUrl: "views/adm/phases/add.html"
    }).when("/adm/phases/list", {
        controller : "phasesController",
        templateUrl: "views/adm/phases/list.html"
    }).when("/adm/phases", {
        controller : "phasesController",
        templateUrl: "views/adm/phases/list.html"
    }).when("/adm/phases/edit/:phase_id", {
        controller : "phasesController",
        templateUrl: "views/adm/phases/edit.html"
    }).when("/adm/phases/documents/:phase_id", {
        controller : "phasesController",
        templateUrl: "views/adm/phases/documents.html"
    }).when("/adm/document-types/add", {                        // documentType
        controller : "documentTypesController",
        templateUrl: "views/adm/documentTypes/add.html"
    }).when("/adm/document-types/list", {
        controller : "documentTypesController",
        templateUrl: "views/adm/documentTypes/list.html"
    }).when("/adm/document-types", {
        controller : "documentTypesController",
        templateUrl: "views/adm/documentTypes/list.html"
    }).when("/adm/document-types/edit/:document_type_id", {
        controller : "documentTypesController",
        templateUrl: "views/adm/documentTypes/edit.html"
    }).when("/adm/documents/add", {                        // documents
        controller : "documentsController",
        templateUrl: "views/adm/documents/add.html"
    }).when("/adm/documents/list", {
        controller : "documentsController",
        templateUrl: "views/adm/documents/list.html"
    }).when("/adm/documents", {
        controller : "documentsController",
        templateUrl: "views/adm/documents/list.html"
    }).when("/adm/documents/edit/:document_id", {
        controller : "documentsController",
        templateUrl: "views/adm/documents/edit.html"
    }).when("/adm/agents/add", {                        // agents
        controller : "agentsController",
        templateUrl: "views/adm/agents/add.html"
    }).when("/adm/agents/list", {
        controller : "agentsController",
        templateUrl: "views/adm/agents/list.html"
    }).when("/adm/agents", {
        controller : "agentsController",
        templateUrl: "views/adm/agents/list.html"
    }).when("/adm/agents/edit/:agent_id", {
        controller : "agentsController",
        templateUrl: "views/adm/agents/edit.html"
    }).when("/adm/agent-types/add", {                        // agentTypes
        controller : "agentTypesController",
        templateUrl: "views/adm/agentTypes/add.html"
    }).when("/adm/agent-types/list", {
        controller : "agentTypesController",
        templateUrl: "views/adm/agentTypes/list.html"
    }).when("/adm/agent-types", {
        controller : "agentTypesController",
        templateUrl: "views/adm/agentTypes/list.html"
    }).when("/adm/agent-types/edit/:agent_type_id", {
        controller : "agentTypesController",
        templateUrl: "views/adm/agentTypes/edit.html"
    }).when("/adm/project-types/add", {                        // projectTypes
        controller : "projectTypesController",
        templateUrl: "views/adm/projectTypes/add.html"
    }).when("/adm/project-types/list", {
        controller : "projectTypesController",
        templateUrl: "views/adm/projectTypes/list.html"
    }).when("/adm/project-types", {
        controller : "projectTypesController",
        templateUrl: "views/adm/projectTypes/list.html"
    }).when("/adm/project-types/edit/:project_type_id", {
        controller : "projectTypesController",
        templateUrl: "views/adm/projectTypes/edit.html"
    }).when("/adm/contractor-types/add", {                        // contractorTypes
        controller : "contractorTypesController",
        templateUrl: "views/adm/contractorTypes/add.html"
    }).when("/adm/contractor-types/list", {
        controller : "contractorTypesController",
        templateUrl: "views/adm/contractorTypes/list.html"
    }).when("/adm/contractor-types", {
        controller : "contractorTypesController",
        templateUrl: "views/adm/contractorTypes/list.html"
    }).when("/adm/contractor-types/edit/:contractor_type_id", {
        controller : "contractorTypesController",
        templateUrl: "views/adm/contractorTypes/edit.html"
    }).when("/adm/contractors/add", {                        // contractors
        controller : "contractorsController",
        templateUrl: "views/adm/contractors/add.html"
    }).when("/adm/contractors/list", {
        controller : "contractorsController",
        templateUrl: "views/adm/contractors/list.html"
    }).when("/adm/contractors", {
        controller : "contractorsController",
        templateUrl: "views/adm/contractors/list.html"
    }).when("/adm/contractors/edit/:contractor_id", {
        controller : "contractorsController",
        templateUrl: "views/adm/contractors/edit.html"
    }).when("/adm/projects/add", {                        // projects
        controller : "projectsController",
        templateUrl: "views/adm/projects/add.html"
    }).when("/adm/projects/list", {
        controller : "projectsController",
        templateUrl: "views/adm/projects/list.html"
    }).when("/adm/projects", {
        controller : "projectsController",
        templateUrl: "views/adm/projects/list.html"
    }).when("/adm/projects/edit/:project_id", {
        controller : "projectsController",
        templateUrl: "views/adm/projects/edit.html"
    }).when("/adm/locations/add", {                        // locations
        controller : "locationsController",
        templateUrl: "views/adm/locations/add.html"
    }).when("/adm/locations/list", {
        controller : "locationsController",
        templateUrl: "views/adm/locations/list.html"
    }).when("/adm/locations", {
        controller : "locationsController",
        templateUrl: "views/adm/locations/list.html"
    }).when("/adm/locations/edit/:location_id", {
        controller : "locationsController",
        templateUrl: "views/adm/locations/edit.html"
    }).when("/login/menu/:token", {                           // loginMenu
      controller : "loginController",
      templateUrl: "views/login/menu.html"
    }).when("/login/salir", {
      controller : "loginController",
      templateUrl: "views/login/salir.html"
    });

}
//creamos el modulo y le aplicamos la configuración
var Acl = angular.module("Acl", ['ui.bootstrap','ngRoute','checklist-model']).config(configAcl);

Acl.directive('inputDescription', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>';
  element = element + '<input id="description" class="form-control input-lg" name="description"';
  element = element + 'placeholder="Description" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.description" required>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});
Acl.directive('inputDescriptionUrl', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>';
  element = element + '<input id="description" class="form-control input-lg" name="description"';
  element = element + 'placeholder="Description" type="text" ng-model="formData.description" required>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});


Acl.directive('inputDescriptionLarge', function() {
  var element =       '<div class="input-group col-sm-12 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>';
  element = element + '<input id="description" class="form-control input-lg" name="description"';
  element = element + 'placeholder="Description" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.description" required>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});



Acl.directive('inputMethod', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>';
  element = element + '<input id="method" class="form-control input-lg" name="method"';
  element = element + 'placeholder="Metodo" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.method" required>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});


Acl.directive('inputPort', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>';
  element = element + '<input id="port" class="form-control input-lg" name="port"';
  element = element + 'placeholder="Puerto" type="text" ng-model="formData.port"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});



Acl.directive('textareaNotes', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>';
  element = element + '<textarea id="notes" class="form-control input-lg" name="notes"';
  element = element + 'placeholder="Notes" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.notes" rows="5"></textarea>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});


Acl.directive('textareaNotesDosColumn', function() {
  var element =       '<div class="input-group col-sm-10 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>';
  element = element + '<textarea id="notes" class="form-control input-lg" name="notes"';
  element = element + 'placeholder="Notes" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.notes" rows="5"></textarea>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('btnAdd', function() {
  var element =       '<div class="col-sm-12 well-sm ">';
  element = element + '<button type="submit" class="btn btn-default " ng-model="btnAdd">Guardar</button>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('iconAdd', function() {
  var element =       '<center class="icon">';
  element = element + '<a href="#/{{sectionSubMenu}}/add" title="Agregar">';
  element = element + '<i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>';
  element = element + '</a>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('iconList', function() {
  var element =       '<center class="icon">';
  element = element + '<a href="#/{{sectionSubMenu}}/list">';
  element = element + '<i class="fa fa-list-alt fa-2x" aria-hidden="true"></i>';
  element = element + '</a>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('panelTitle', function() {
  var element =       '<div class="textTitle">';
  element = element + '<h2>{{sectionTitle}} / {{actionTitle}}</h2>';
  element = element + '<hr>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});
Acl.directive('panelTitleDos', function() {
  var element =       '<div class="textTitle">';
  element = element + '<h2>{{sectionTitle}}</h2>';
  element = element + '<hr>';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('textSearchTable', function() {
  var element =       '<div class="input-group well-sm col-sm-5 searchbar">';
  element = element+  '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>';
  element = element + '<input type="text" ng-model="filter" ng-change="filterByMe()"  class="form-control input-sm" name="description" placeholder="Buscar">';
  return {
     restrict: 'AE',
    template: element
  };
 });




Acl.directive("alertMessage", function($compile) {
    return {
        scope: {
            alert: "="
        },
        link: function (scope, element) {
            // Actualiza el mensaje de alerta cada vez que el objeto es modificado.
            scope.$watch('alert', function () {
                updateAlert();
            });

            // Cerrar mensaje de alerta
            scope.close = function() {
                scope.alert = null;
            }

            function updateAlert() {
                var html = "";

                if (scope.alert) {
                    var icon = null;

                    switch (scope.alert.type) {
                        case 'success': {
                            icon = 'ok-sign';
                        } break;
                        case 'warning': {
                            icon = 'exclamation-sign';
                        } break;
                        case 'info': {
                            icon = 'info-sign';
                        } break;
                        case 'danger': {
                            icon = 'remove-sign';
                        } break;
                    }

                    html = "<div class='alert alert-" + scope.alert.type + "' role='alert'>";

                    if (scope.alert.closable) {
                        html += "<button type='button' class='close' data-dismiss='alert' ng-click='close()' aria-label='Close'><span aria-hidden='true'></span></button>";
                    }

                    if (icon) {
                        html += "<span style='padding-right: 5px;' class='glyphicon glyphicon-" + icon + "' aria-hidden='true'></span>";
                    }

                    html += scope.alert.text;
                    html += "</div>";
                }

                var newElement = angular.element(html);
                var compiledElement = $compile(newElement)(scope);

                element.html(compiledElement);

                if (scope.alert && scope.alert.delay > 0) {
                    setTimeout(function () {
                        scope.alert = null;
                        scope.$apply();
                    }, scope.alert.delay * 1000);
                }
            }
        }
     }
});




Acl.directive('inputName', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>';
  element = element + '<input id="name" class="form-control input-lg" name="name"';
  element = element + 'placeholder="Nombre" onkeyup="changeToUpperCase(this)" type="text" required ng-model="formData.name"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputLastName', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>';
  element = element + '<input id="last_name" class="form-control input-lg" name="last_name"';
  element = element + 'placeholder="Apellido paterno" onkeyup="changeToUpperCase(this)" type="text" required ng-model="formData.last_name"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputSecondLastName', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>';
  element = element + '<input id="second_last_name" class="form-control input-lg" name="second_last_name"';
  element = element + 'placeholder="Apellido materno" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.second_last_name"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputUser', function() {
  var element =       '<div class="input-group col-sm-5 well-sm col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>';
  element = element + '<input id="user" required class="form-control input-lg" name="user"';
  element = element + 'placeholder="Usuario" type="text" ng-model="formData.user"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});



Acl.directive('iconReturnCitieState', function() {
  var element =       '<a href="#/{{sectionSubMenu}}/states">';
  element = element + '<i class="fa fa-list-alt fa-2x"  aria-hidden="true"></i>';
  element = element + '</a>';
  return {
     restrict: 'AE',
    template: element
  };
});



Acl.directive('inputPassword', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>';
  element = element + '<input id="password" required class="form-control input-lg" name="password"';
  element = element + 'placeholder="Password" type="password" ng-model="formData.password"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputPhoneNumber', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">phone_iphone</i></span>';
  element = element + '<input id="phone_number" class="form-control input-lg" name="phone_number"';
  element = element + 'placeholder="Número de celular" type="text" required ng-model="formData.phone_number"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputCp', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">perm_contact_calendar</i></span>';
  element = element + '<input id="cp" class="form-control input-lg" name="cp"';
  element = element + 'placeholder="Codigo postal" type="text" ng-model="formData.cp"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputStreet', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-address-card-o" aria-hidden="true"></i></span>';
  element = element + '<input id="street" class="form-control input-lg" name="street"';
  element = element + 'placeholder="Calle" type="text" onkeyup="changeToUpperCase(this)" ng-model="formData.street"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputColony', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-address-card-o" aria-hidden="true"></i></span>';
  element = element + '<input id="colony" class="form-control input-lg" name="colony"';
  element = element + 'placeholder="Colonia" type="text" onkeyup="changeToUpperCase(this)" ng-model="formData.colony"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputStreetAddress', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-address-card-o" aria-hidden="true"></i></span>';
  element = element + '<input id="street_address" class="form-control input-lg" name="street_address"';
  element = element + 'placeholder="Número exterior" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.street_address"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputApartmentNumber', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-address-card-o" aria-hidden="true"></i></span>';
  element = element + '<input id="apartment_number" class="form-control input-lg" name="apartment_number"';
  element = element + 'placeholder="Número interior" onkeyup="changeToUpperCase(this)" type="text" ng-model="formData.apartment_number"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputLot', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-address-card-o" aria-hidden="true"></i></span>';
  element = element + '<input id="lot" class="form-control input-lg" name="lot"';
  element = element + 'placeholder="Lote" type="text" onkeyup="changeToUpperCase(this)" ng-model="formData.lot"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});
Acl.directive('inputBlock', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-address-card-o" aria-hidden="true"></i></span>';
  element = element + '<input id="block" class="form-control input-lg" name="block"';
  element = element + 'placeholder="Manzana" type="text" onkeyup="changeToUpperCase(this)" ng-model="formData.block"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputEmail', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">mail_outline</i></span>';
  element = element + '<input id="email" class="form-control input-lg" name="email"';
  element = element + 'placeholder="Correo" type="email" ng-model="formData.email"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputCurp', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">perm_contact_calendar</i></span>';
  element = element + '<input id="curp" class="form-control input-lg" name="curp"';
  element = element + 'placeholder="curp" type="text" onkeyup="changeToUpperCasewithoutSpace(this)" required ng-model="formData.curp"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});


Acl.directive('inputRfc', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">perm_contact_calendar</i></span>';
  element = element + '<input id="rfc" class="form-control input-lg" name="rfc"';
  element = element + 'placeholder="rfc" type="text" onkeyup="changeToUpperCasewithoutSpace(this)" required ng-model="formData.rfc"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputFile', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">attach_file</i></span>';
  element = element + '<input id="file" class="form-control input-lg" name="file"';
  element = element + 'placeholder="File" type="text" required ng-model="formData.file"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});


Acl.directive('inputBirthdate', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-birthday-cake" aria-hidden="true"></i></span>';
  element = element + '<input id="birthdate" class="form-control input-lg" name="date"';
  element = element + 'placeholder="Fecha de nacimiento" type="date" required  ng-model="formData.birthdate"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});
Acl.directive('inputPlaceOfBirth', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-hospital-o" aria-hidden="true"></i></span>';
  element = element + '<input id="place_of_birth" class="form-control input-lg" name="place_of_birth"';
  element = element + 'placeholder="Lugar de nacimiento" onkeyup="changeToUpperCase(this)" type="text" required ng-model="formData.place_of_birth"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});
Acl.directive('inputPurchaseRangeInitial', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">monetization_on</i></span>';
  element = element + '<input id="purchase_range_initial" class="form-control input-lg" name="purchase_range_initial"';
  element = element + 'placeholder="Rango compra inicial" type="text" required ng-model="formData.purchase_range_initial"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});
Acl.directive('inputPurchaseRangeFinal', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="material-icons">monetization_on</i></span>';
  element = element + '<input id="purchase_range_final" class="form-control input-lg" name="purchase_range_final"';
  element = element + 'placeholder="Rango compra final" type="text" required ng-model="formData.purchase_range_final"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});

Acl.directive('inputBusinessName', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true""></i></span>';
  element = element + '<input id="business_name" class="form-control input-lg" name="business_name"';
  element = element + 'placeholder="Razon social" type="text" required ng-model="formData.business_name"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});
Acl.directive('inputLegalEntity', function() {
  var element =       '<div class="input-group col-sm-5 well-lg col-centered">';
  element = element + '<span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true""></i></span>';
  element = element + '<input id="legal_entity" class="form-control input-lg" name="legal_entity"';
  element = element + 'placeholder="Persona moral" type="text" required ng-model="formData.legal_entity"';
  element = element + '</div>';
  return {
     restrict: 'AE',
    template: element
  };
});


angular.module('checklist-model', [])
.directive('checklistModel', ['$parse', '$compile', function($parse, $compile) {
  // contains
  function contains(arr, item, comparator) {
    if (angular.isArray(arr)) {
      for (var i = arr.length; i--;) {
        if (comparator(arr[i], item)) {
          return true;
        }
      }
    }
    return false;
  }

  // add
  function add(arr, item, comparator) {
    arr = angular.isArray(arr) ? arr : [];
      if(!contains(arr, item, comparator)) {
          arr.push(item);
      }
    return arr;
  }

  // remove
  function remove(arr, item, comparator) {
    if (angular.isArray(arr)) {
      for (var i = arr.length; i--;) {
        if (comparator(arr[i], item)) {
          arr.splice(i, 1);
          break;
        }
      }
    }
    return arr;
  }

  // http://stackoverflow.com/a/19228302/1458162
  function postLinkFn(scope, elem, attrs) {
     // exclude recursion, but still keep the model
    var checklistModel = attrs.checklistModel;
    attrs.$set("checklistModel", null);
    // compile with `ng-model` pointing to `checked`
    $compile(elem)(scope);
    attrs.$set("checklistModel", checklistModel);

    // getter / setter for original model
    var getter = $parse(checklistModel);
    var setter = getter.assign;
    var checklistChange = $parse(attrs.checklistChange);
    var checklistBeforeChange = $parse(attrs.checklistBeforeChange);

    // value added to list
    var value = attrs.checklistValue ? $parse(attrs.checklistValue)(scope.$parent) : attrs.value;


    var comparator = angular.equals;

    if (attrs.hasOwnProperty('checklistComparator')){
      if (attrs.checklistComparator[0] == '.') {
        var comparatorExpression = attrs.checklistComparator.substring(1);
        comparator = function (a, b) {
          return a[comparatorExpression] === b[comparatorExpression];
        };

      } else {
        comparator = $parse(attrs.checklistComparator)(scope.$parent);
      }
    }

    // watch UI checked change
    scope.$watch(attrs.ngModel, function(newValue, oldValue) {
      if (newValue === oldValue) {
        return;
      }

      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
        scope[attrs.ngModel] = contains(getter(scope.$parent), value, comparator);
        return;
      }

      setValueInChecklistModel(value, newValue);

      if (checklistChange) {
        checklistChange(scope);
      }
    });

    function setValueInChecklistModel(value, checked) {
      var current = getter(scope.$parent);
      if (angular.isFunction(setter)) {
        if (checked === true) {
          setter(scope.$parent, add(current, value, comparator));
        } else {
          setter(scope.$parent, remove(current, value, comparator));
        }
      }

    }

    // declare one function to be used for both $watch functions
    function setChecked(newArr, oldArr) {
      if (checklistBeforeChange && (checklistBeforeChange(scope) === false)) {
        setValueInChecklistModel(value, scope[attrs.ngModel]);
        return;
      }
      scope[attrs.ngModel] = contains(newArr, value, comparator);
    }

    // watch original model change
    // use the faster $watchCollection method if it's available
    if (angular.isFunction(scope.$parent.$watchCollection)) {
        scope.$parent.$watchCollection(checklistModel, setChecked);
    } else {
        scope.$parent.$watch(checklistModel, setChecked, true);
    }
  }

  return {
    restrict: 'A',
    priority: 1000,
    terminal: true,
    scope: true,
    compile: function(tElement, tAttrs) {
      if ((tElement[0].tagName !== 'INPUT' || tAttrs.type !== 'checkbox') && (tElement[0].tagName !== 'MD-CHECKBOX') && (!tAttrs.btnCheckbox)) {
        throw 'checklist-model should be applied to `input[type="checkbox"]` or `md-checkbox`.';
      }

      if (!tAttrs.checklistValue && !tAttrs.value) {
        throw 'You should provide `value` or `checklist-value`.';
      }

      // by default ngModel is 'checked', so we set it if not specified
      if (!tAttrs.ngModel) {
        // local scope var storing individual checkbox model
        tAttrs.$set("ngModel", "checked");
      }

      return postLinkFn;
    }
  };
}]);
