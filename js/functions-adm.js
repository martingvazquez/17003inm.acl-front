
function getClients($location,url,$scope,$http, otherController = false){
  $http.get(url).success(function(clientsData) {
  var error = clientsData.error;
  if(error == 4){
    $location.path('/login/denegado');
  }

  $scope.clients = clientsData.clients;
  $scope.totalItems = clientsData.n_rows;
  console.log($scope.totalItems);
  if(!otherController){
    if(clientsData.n_rows>=1){

      $scope.formData.name = $scope.clients[0].name;
      $scope.formData.last_name = $scope.clients[0].last_name;
      $scope.formData.second_last_name = $scope.clients[0].second_last_name;
      $scope.formData.phone_number = $scope.clients[0].phone_number;
      $scope.formData.email = $scope.clients[0].email;
      $scope.formData.curp = $scope.clients[0].curp;
      $scope.formData.rfc = $scope.clients[0].rfc;
      $scope.formData.notes = $scope.clients[0].notes;
      $scope.formData.gender = $scope.clients[0].gender;
      $scope.formData.birth_date = $scope.clients[0].birth_date;
      $scope.formData.place_of_birth = $scope.clients[0].place_of_birth;
      $scope.formData.purchase_range_initial = $scope.clients[0].purchase_range_initial;
      $scope.formData.purchase_range_final = $scope.clients[0].purchase_range_final;

      $scope.formData.client_type_id = $scope.clients[0].client_type_id;
      $scope.formData.financing_id = $scope.clients[0].financing_id;
      $scope.formData.agent_id = $scope.clients[0].agent_id;

      $scope.date_select = $scope.clients[0].birthdate;
      $scope.gender_select = 0;

      $scope.client_type_id_select = $scope.clients[0].client_type_id;
      $scope.client_type_select = $scope.clients[0].client_type;

      $scope.financing_id_select = $scope.clients[0].financing_id;
      $scope.financing_select = $scope.clients[0].financing;

      $scope.financing_type_id_select = $scope.clients[0].financing_type_id;
      $scope.financing_type_select = $scope.clients[0].financing_type;

      $scope.agent_id_select = $scope.clients[0].agent_id;
      $scope.agent_select = $scope.clients[0].agent;

      getClientTypes('/acl/client-types/token/'+token+'/filter/client_types.status/1',$scope,$http,true);
      getFinancingTypes('/acl/financing-types/token/'+token+'/filter/financing_type.status/1',$scope,$http,true);
      getAgents('/acl/agents/token/'+token+'/filter/agents.status/1',$scope,$http,true);
      var financing_type_id = $scope.clients[0].financing_type_id;
      getFinancings('/acl/financings/token/'+token+'/filters2/financings.financing_type_id/'+financing_type_id+'/financings.status/1',$scope,$http,true);

    } else {
      $scope.formData.name = $scope.clients.name= '';
      $scope.formData.last_name = $scope.clients.last_name= '';
      $scope.formData.second_last_name = $scope.clients.second_last_name= '';
      $scope.formData.phone_number = $scope.clients.phone_number= '';
      $scope.formData.email = $scope.clients.email= '';
      $scope.formData.curp = $scope.clients.curp= '';
      $scope.formData.rfc = $scope.clients.rfc= '';
      $scope.formData.notes = '';
      $scope.formData.gender = '';
      $scope.formData.birth_date = '';
      $scope.formData.place_of_birth = '';
      $scope.formData.purchase_range_initial = '';
      $scope.formData.purchase_range_final = '';
      $scope.client_type_id_select = '';
      $scope.client_type_select = '';
      $scope.financing_id_select = '';
      $scope.financing_select = '';
    }
  }
    });
}

function getClientTypes(url,$scope,$http,otherController = false){
    $http.get(url).success(function(clientTypesData) {
        $scope.clientTypes = clientTypesData.client_types;
        $scope.totalItems = clientTypesData.n_rows;
        if(!otherController){
          if(clientTypesData.n_rows>=1){
            $scope.formData.description = $scope.clientTypes[0].description;
            $scope.formData.notes = $scope.clientTypes[0].notes;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
          }
        }
    });
}



function getAgentTypes(url,$scope,$http,otherController = false){
    $http.get(url).success(function(agentTypesData) {
        $scope.agentTypes = agentTypesData.agent_types;
        $scope.totalItems = agentTypesData.n_rows;
        if(!otherController){
          if(agentTypesData.n_rows>=1){
            $scope.formData.description = $scope.agentTypes[0].description;
            $scope.formData.notes = $scope.agentTypes[0].notes;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
          }
        }
    });
}


function getFinancings(url,$scope,$http, otherController = false){
    $http.get(url).success(function(financingsData) {
      $scope.financings = financingsData.financings;
      if (!otherController) {
        $scope.totalItems =financingsData.n_rows;
        if(financingsData.n_rows>=1){
          $scope.financing_type_id_select = 0;
          $scope.financing_type_select = '';

          $scope.formData.description = $scope.financings[0].description;
          $scope.formData.notes = $scope.financings[0].notes;

          $scope.formData.financing_type_id = $scope.financings[0].financing_type_id;

          $scope.financing_type_id_select = $scope.financings[0].financing_type_id;
          $scope.financing_type_select = $scope.financings[0].financing_type;
        } else {
          $scope.formData.rfc = $scope.financings.description= '';
          $scope.formData.notes = '';
          $scope.financing_type_id_select = '';
          $scope.financing_type_select = '';
        }
      }
    });
}
function getFinancingTypes(url,$scope,$http, otherController = false){
    $http.get(url).success(function(financingTypesData) {
      $scope.financingTypes = financingTypesData.financing_types;
      $scope.totalItems =financingTypesData.n_rows;
      if(!otherController){
        if(financingTypesData.n_rows>=1){

          $scope.formData.description = $scope.financingTypes[0].description;
          $scope.formData.notes = $scope.financingTypes[0].notes;
        } else {
          $scope.formData.description = 'No existe Registros';
          $scope.formData.notes = '';
        }
      }
    });
}

function getPhases(url,$scope,$http,otherController = false){
    $http.get(url).success(function(phasesData) {
        $scope.phases = phasesData.phases;
        $scope.totalItems = phasesData.n_rows;
        if(!otherController){
          if(phasesData.n_rows>=1){
            $scope.formData.description = $scope.phases[0].description;
            $scope.formData.notes = $scope.phases[0].notes;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
          }
        }
    });
}

function getDocumentTypes(url,$scope,$http,otherController = false){
    $http.get(url).success(function(documentTypesData) {
        $scope.documentTypes = documentTypesData.document_types;
        $scope.totalItems =documentTypesData.n_rows;
        if(!otherController){
          if(documentTypesData.n_rows>=1){
            $scope.formData.description = $scope.documentTypes[0].description;
            $scope.formData.notes = $scope.documentTypes[0].notes;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
          }
        }
    });
}

function getDocuments(url,$scope,$http,otherController = false){
    $http.get(url).success(function(documentsData) {
        $scope.documents = documentsData.documents;
        $scope.totalItems =documentsData.n_rows;
        if(!otherController){
          if(documentsData.n_rows>=1){
            $scope.document_type_id_select = 0;
            $scope.document_type_select = '';

            $scope.formData.description = $scope.documents[0].description;
            $scope.formData.notes = $scope.documents[0].notes;
            $scope.formData.file = $scope.documents[0].file;

            $scope.formData.document_type_id = $scope.documents[0].document_type_id;

            $scope.document_type_id_select = $scope.documents[0].document_type_id;
            $scope.document_type_select = $scope.documents[0].document_type;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
            $scope.formData.file = '';
          }
        }
    });
}

function getProjectTypes(url,$scope,$http,otherController = false){
    $http.get(url).success(function(projectTypesData) {
        $scope.projectTypes = projectTypesData.project_types;
        $scope.totalItems =projectTypesData.n_rows;
        if(!otherController){
          if(projectTypesData.n_rows>=1){
            $scope.formData.description = $scope.projectTypes[0].description;
            $scope.formData.notes = $scope.projectTypes[0].notes;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
          }
        }
    });
}

function getProjects(url,$scope,$http,otherController = false){
    $http.get(url).success(function(projectsData) {
        $scope.projects = projectsData.projects;
        if(!otherController){
          $scope.totalItems = projectsData.n_rows;
          if(projectsData.n_rows>=1){


            $scope.formData.description = $scope.projects[0].description;
            $scope.formData.notes = $scope.projects[0].notes;
            $scope.formData.cp = $scope.projects[0].cp;
            $scope.formData.street = $scope.projects[0].street;
            $scope.formData.colony = $scope.projects[0].colony;
            $scope.formData.street_address = $scope.projects[0].street_address;
            $scope.formData.apartment_number = $scope.projects[0].apartment_number;

            $scope.formData.project_type_id = $scope.projects[0].project_type_id;
            $scope.formData.contractor_id = $scope.projects[0].contractor_id;

            $scope.project_type_id_select = $scope.projects[0].project_type_id;
            $scope.project_type_select = $scope.projects[0].project_type;

            $scope.contractor_id_select = $scope.projects[0].contractor_id;
            $scope.contractor_select = $scope.projects[0].contractor;

            $scope.contractor_type_id_select = $scope.projects[0].contractor_type_id;
            $scope.contractor_type_select = $scope.projects[0].contractor_type;


            getProjectTypes('/acl/project-types/token/'+token+'/filter/project_types.status/1',$scope,$http,true);
            getContractorTypes('/acl/contractor-types/token/'+token+'/filter/contractor_type.status/1',$scope,$http,true);
            var contractor_type_id = $scope.projects[0].contractor_type_id;
            getContractors('/acl/contractors/token/'+token+'/filters2/contractors.contractor_type_id/'+contractor_type_id+'/contractor.status/1',$scope,$http,true);

          } else {

            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
            $scope.formData.cp = ''
            $scope.formData.street = '';
            $scope.formData.colony = '';
            $scope.formData.street_address = '';
            $scope.formData.apartment_number = '';
            $scope.project_type_id_select = '';
            $scope.project_type_select = '';
            $scope.contractor_id_select = '';
            $scope.contractor_select = '';

          }
        }
    });
}

function getLocations(url,$scope,$http,otherController = false){
    $http.get(url).success(function(locationsData) {
        $scope.locations = locationsData.locations;
        $scope.totalItems = locationsData.n_rows;
        if(!otherController){
          if(locationsData.n_rows>=1){
            $scope.project_id_select = 0;
            $scope.project_select = '';

            $scope.formData.description = $scope.locations[0].description;
            $scope.formData.notes = $scope.locations[0].notes;
            $scope.formData.cp = $scope.locations[0].cp;
            $scope.formData.street = $scope.locations[0].street;
            $scope.formData.colony = $scope.locations[0].colony;
            $scope.formData.street_address = $scope.locations[0].street_address;
            $scope.formData.apartment_number = $scope.locations[0].apartment_number;
            $scope.formData.lot = $scope.locations[0].lot;
            $scope.formData.block = $scope.locations[0].block;

            $scope.formData.project_id = $scope.locations[0].project_id;
            $scope.project_id_select = $scope.locations[0].project_id;
            $scope.project_select = $scope.locations[0].project;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
            $scope.formData.cp = ''
            $scope.formData.street = '';
            $scope.formData.colony = '';
            $scope.formData.street_address = '';
            $scope.formData.apartment_number = '';
            $scope.formData.lot = '';
            $scope.formData.block = '';
          }
        }
    });
}



function getContractorTypes(url,$scope,$http,otherController = false){
    $http.get(url).success(function(contractorTypesData) {
        $scope.contractorTypes = contractorTypesData.contractor_types;
        $scope.totalItems = contractorTypesData.n_rows;
        if(!otherController){
          if(contractorTypesData.n_rows>=1){
            $scope.formData.description = $scope.contractorTypes[0].description;
            $scope.formData.notes = $scope.contractorTypes[0].notes;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
          }
        }
    });
}

function getContractors(url,$scope,$http,otherController = false){
    $http.get(url).success(function(contractorsData) {
        $scope.contractors = contractorsData.contractors;
        $scope.totalItems = contractorsData.n_rows;
        if(!otherController){
          if(contractorsData.n_rows>=1){
            $scope.contractor_type_id_select = 0;
            $scope.contractor_type_select = '';

            $scope.formData.description = $scope.contractors[0].description;
            $scope.formData.notes = $scope.contractors[0].notes;
            $scope.formData.name = $scope.contractors[0].name;
            $scope.formData.last_name = $scope.contractors[0].last_name;
            $scope.formData.second_last_name = $scope.contractors[0].second_last_name;
            $scope.formData.phone_number = $scope.contractors[0].phone_number;
            $scope.formData.email = $scope.contractors[0].email;
            $scope.formData.curp = $scope.contractors[0].curp;
            $scope.formData.rfc = $scope.contractors[0].rfc;
            $scope.formData.cp = $scope.contractors[0].cp;
            $scope.formData.street = $scope.contractors[0].street;
            $scope.formData.colony = $scope.contractors[0].colony;
            $scope.formData.street_address = $scope.contractors[0].street_address;
            $scope.formData.apartment_number = $scope.contractors[0].apartment_number;
            $scope.formData.lot = $scope.contractors[0].business_name;
            $scope.formData.block = $scope.contractors[0].legal_entity;

            $scope.formData.contractor_type_id = $scope.contractors[0].contractor_type_id;

            $scope.contractor_type_id_select = $scope.contractors[0].contractor_type_id;
            $scope.contractor_type_select = $scope.contractors[0].contractor_type;
          } else {
            $scope.formData.description = 'No existe Registros';
            $scope.formData.notes = '';
            $scope.formData.name = '';
            $scope.formData.last_name = '';
            $scope.formData.second_last_name = '';
            $scope.formData.phone_number = '';
            $scope.formData.email = '';
            $scope.formData.curp = '';
            $scope.formData.rfc = '';
            $scope.formData.cp = ''
            $scope.formData.street = '';
            $scope.formData.colony = '';
            $scope.formData.street_address = '';
            $scope.formData.apartment_number = '';
            $scope.formData.business_name = '';
            $scope.formData.legal_entity = '';
          }
        }
    });
}


function getAgents(url,$scope,$http, otherController = false){
    $http.get(url).success(function(agentsData) {
        $scope.agents = agentsData.agents;
        if(!otherController){
          $scope.totalItems = agentsData.n_rows;
          if(agentsData.n_rows>=1){
            $scope.agent_type_id_select = 0;
            $scope.agent_type_select = '';

            $scope.formData.name = $scope.agents[0].name;
            $scope.formData.last_name = $scope.agents[0].last_name;
            $scope.formData.second_last_name = $scope.agents[0].second_last_name;
            $scope.formData.phone_number = $scope.agents[0].phone_number;
            $scope.formData.email = $scope.agents[0].email;
            $scope.formData.curp = $scope.agents[0].curp;
            $scope.formData.rfc = $scope.agents[0].rfc;
            $scope.formData.notes = $scope.agents[0].notes;

            $scope.formData.agent_type_id = $scope.agents[0].agent_type_id;

            $scope.agent_type_id_select = $scope.agents[0].agent_type_id;
            $scope.agent_type_select = $scope.agents[0].agent_type;
          } else {
            $scope.formData.name = '';
            $scope.formData.last_name = '';
            $scope.formData.second_last_name = '';
            $scope.formData.phone_number = '';
            $scope.formData.email = '';
            $scope.formData.curp = '';
            $scope.formData.rfc = '';
            $scope.formData.notes = '';
            $scope.agent_type_id_select = '';
            $scope.agent_type_select = '';
          }
        }
    });
}
