$("#menu-toggle").click(function(e) {
       e.preventDefault();
       $("#sidebar-wrapper").toggleClass("toggled");
       $(".shadow").toggleClass("shadow-active");
       $("#page-content-wrapper").toggleClass("fixed");
});

$(".shadow").click(function() {
       $("#sidebar-wrapper").addClass("toggled");
       $(".shadow").removeClass("shadow-active");
       $("#page-content-wrapper").removeClass("fixed");
});

$(".close-menu").click(function() {
       $("#sidebar-wrapper").addClass("toggled");
       $(".shadow").removeClass("shadow-active");
       $("#page-content-wrapper").removeClass("fixed");
});

function menu() {
  location.href = "#/login/bienvenido";
  location.reload();
}

function clients() {
  $("#inm-adm").removeClass("not-visible");
  $("#inm-acl").addClass("not-visible");
  location.href= "#/adm/login/menu-adm";
  location.reload();
}
function acl() {
  $("#inm-acl").removeClass("not-visible");
  $("#inm-adm").addClass("not-visible");
  location.href = "#/acl/login/menu-acl";
  location.reload();
}

function changeToUpperCase(t) {
   var eleVal = document.getElementById(t.id);
   eleVal.value = eleVal.value.toUpperCase();
}
function changeToUpperCasewithoutSpace(t) {
   var eleVal = document.getElementById(t.id);
   eleVal.value= eleVal.value.toUpperCase().replace(/ /g,'');
}


function getToken(){

  if(document.cookie!=''){

    tokens = document.cookie.split('=');
    token = tokens[1];
    return token;

  }
  else{
    return false;
  }
}
function splitUrl(url){
    if(url == "" || url ==null){
        return "";
    }
    else{
       url = url.split('/');
        return url;
    }
}
function FirstUpperCase(string){
    if(string=="" || string==null){
        return "";
    }
    else{
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}


function getGroupsPermission(url,$scope,$http,group_id){

  $http.get(url).success(function(data) {

    $scope.group = data.group;
    $scope.applications = data.applications;


  });


}


function getGroupsUrls(url,$scope,$http,group_id){
  $scope.url = {};
  $scope.url['permissions']=[];
  $http.get(url).success(function(urlsData) {
    $scope.urls  = urlsData.urls;
    $scope.totalItems   = urlsData.n_rows;
    $scope.urlsView = [];
    $scope.ur ={};
    $scope.ur.urlsView = [];
    j=0;
    for(i = 0; i<$scope.totalItems; i++){
      $scope.urlsView[i] = $scope.urls[i];
      if($scope.urls[i].existe==1){
        $scope.ur.urlsView[j] = $scope.urls[i].url_id;
        j++;
      }
    }
  });
}

function getCompanies($location,url,$scope,$http,otherController=false){
    $http.get(url).success(function(companiesData) {
        var error = companiesData.error;
        if(error == 4){
         $location.path('/login/denegado');
        }
        $scope.companies = companiesData.companies;
        $scope.totalItems =companiesData.n_rows;
        if(!otherController){
          if(companiesData.n_rows>=1){
            $scope.formData.description = $scope.companies[0].description;
            $scope.formData.notes = $scope.companies[0].notes;
            $scope.formData.city_id = $scope.companies[0].city_id;

            $scope.country_id_select = $scope.companies[0].country_id;
            $scope.country_select = $scope.companies[0].country;

            $scope.city_id_select = $scope.companies[0].city_id;
            $scope.city_select = $scope.companies[0].city;

            $scope.state_id_select = $scope.companies[0].state_id;
            $scope.state_select = $scope.companies[0].state;

            getCountries($location,'/acl/countries/token/'+token+'/filter/countries.status/1',$scope,$http,true);
            var country_id = $scope.companies[0].country_id;
            var url = '/acl/states/token/'+token+'/filters2/states.country_id/'+country_id+'/states.status/1';
            getStates($location,url,$scope,$http,true);
            var state_id = $scope.companies[0].state_id;
            getCities($location,'/acl/cities/token/'+token+'/filters2/cities.state_id/'+state_id+'/cities.status/1',$scope,$http,true);

          }
          else{
            $scope.formData.description = 'No existen Registros';
            $scope.formData.notes = '';
          }
        }
    });
}

function getCountry($location,url,$scope,$http){


  $http.get(url).success(function(countriesData) {
    var error = countriesData.error;
    if(error == 4){
     $location.path('/login/denegado');
    }
    $scope.countries = countriesData.countries;
    $scope.totalItems =countriesData.n_rows;
    if(countriesData.n_rows>=1){
      $scope.country = $scope.countries[0].description;
    }
    else{
      $scope.country = 'No existen Registros';
    }
  });

}


function getState($location,url,$scope,$http){


  $http.get(url).success(function(statesData) {
    var error = statesData.error;
    if(error == 4){
     $location.path('/login/denegado');
    }
    $scope.states = statesData.states;
    $scope.totalItems =statesData.n_rows;
    if(statesData.n_rows>=1){
      $scope.state = $scope.states[0].description;
    }
    else{
      $scope.state = 'No existen Registros';
    }
  });

}


function getCountries($location,url,$scope,$http,otherController = false){
    $http.get(url).success(function(countriesData) {
      var error = countriesData.error;
      if(error == 4){
       $location.path('/login/denegado');
      }
      $scope.countries = countriesData.countries;
      $scope.totalItems =countriesData.n_rows;
        if(!otherController){
          if(countriesData.n_rows>=1){
            $scope.formData.description = $scope.countries[0].description;
            $scope.formData.notes = $scope.countries[0].notes;
          }
          else{
            $scope.formData.description = 'No existen Registros';
            $scope.formData.notes = '';
          }
        }
    });
}


function getApplications($location,url,$scope,$http,otherController = false){
    $http.get(url).success(function(applicationsData) {
      var error = applicationsData.error;
      if(error == 4){
       $location.path('/login/denegado');
      }
      $scope.applications = applicationsData.applications;
      $scope.totalItems =applicationsData.n_rows;

        if(!otherController){
            if(applicationsData.n_rows>=1){
              $scope.formData.description = $scope.applications[0].description;
              $scope.formData.notes = $scope.applications[0].notes;
              $scope.formData.port = $scope.applications[0].port;
            }
            else{
              $scope.formData.description = 'No existen Registros';
              $scope.formData.notes = '';
            }
        }
    });
}


function getGroups($location,url,$scope,$http,otherController = false){
    $http.get(url).success(function(groupsData) {

      var error = groupsData.error;
      if(error == 4){
       $location.path('/login/denegado');
      }


      $scope.groups = groupsData.groups;
      $scope.totalItems =groupsData.n_rows;
        if(!otherController){

            if(groupsData.n_rows>=1){
              $scope.formData.description = $scope.groups[0].description;
              $scope.formData.notes = $scope.groups[0].notes;
              $scope.formData.companie_id = $scope.groups[0].companie_id;

              $scope.companie_id_select = $scope.groups[0].companie_id;
              $scope.companie_select = $scope.groups[0].companie;

            }
            else{
              $scope.formData.description = 'No existen Registros';
              $scope.formData.notes = '';
            }
        }
        else{

        }
    });
}
function getStates($location,url,$scope,$http,otherController = false){
    $http.get(url).success(function(statesData) {
      var error = statesData.error;
      if(error == 4){
       $location.path('/login/denegado');
      }
      $scope.states = statesData.states;
      $scope.totalItems =statesData.n_rows;


      if(!otherController){
        if(statesData.n_rows>=1){
          $scope.formData.description = $scope.states[0].description;
          $scope.formData.notes = $scope.states[0].notes;
          $scope.formData.country_id = $scope.states[0].country_id;
          $scope.country_id_select = $scope.states[0].country_id;
          $scope.country_select = $scope.states[0].country;
          }
          else{
            $scope.formData.description = 'No existen Registros';
            $scope.formData.notes = '';
          }
        }
    });
}


function getUsers($location,url,$scope,$http,otherController = false){
    $http.get(url).success(function(usersData) {
        var error = usersData.error;
        if(error == 4){
         $location.path('/login/denegado');
        }

        $scope.users = usersData.users;
        $scope.totalItems =usersData.n_rows;
        if(!otherController){

          if(usersData.n_rows>=1){

            $scope.formData.description = $scope.users[0].description;
            $scope.formData.notes = $scope.users[0].notes;
            $scope.formData.group_id = $scope.users[0].group_id;
            $scope.formData.name = $scope.users[0].name;
            $scope.formData.last_name = $scope.users[0].last_name;
            $scope.formData.second_last_name = $scope.users[0].second_last_name;
            $scope.formData.user = $scope.users[0].user;
            $scope.formData.password = $scope.users[0].password;


            $scope.group_id_select = $scope.users[0].group_id;
            $scope.group_select = $scope.users[0].group;



          }
          else{
            $scope.formData.description = 'No existen Registros';
            $scope.formData.notes = '';
          }
        }
    });
}

function getResources($location,url,$scope,$http,otherController = false){
    $http.get(url).success(function(resourcesData) {
        var error = resourcesData.error;
        if(error == 4){
         $location.path('/login/denegado');
        }
        $scope.resources = resourcesData.resources;
        $scope.totalItems =resourcesData.n_rows;
        if(!otherController){
          if(resourcesData.n_rows>=1){
            $scope.formData.description = $scope.resources[0].description;
            $scope.formData.notes = $scope.resources[0].notes;
            $scope.formData.application_id = $scope.resources[0].application_id;
            $scope.application_id_select = $scope.resources[0].application_id;
            $scope.application_select = $scope.resources[0].application;
          }
          else{
            $scope.formData.description = 'No existen Registros';
            $scope.formData.notes = '';
          }
        }
    });
}



function getCities($location,url,$scope,$http,otherController = false){

    $http.get(url).success(function(citiesData) {
      var error = citiesData.error;
      if(error == 4){
       $location.path('/login/denegado');
      }
      $scope.cities = citiesData.cities;
      $scope.totalItemsCities =citiesData.n_rows;

        if(!otherController){

          $scope.state_select_id = 0;
          $scope.state_select = '';

          if(citiesData.n_rows >= 1){
            $scope.formData.description = $scope.cities[0].description;
            $scope.formData.notes = $scope.cities[0].notes;
            $scope.formData.state_id = $scope.cities[0].state_id;



            $scope.country_id_select = $scope.cities[0].country_id;
            $scope.country_select = $scope.cities[0].country;


            $scope.state_id_select = $scope.cities[0].state_id;
            $scope.state_select = $scope.cities[0].state;


            getCountries($location,'/acl/countries/token/'+token+'/filter/countries.status/1',$scope,$http,true);
            var country_id = $scope.cities[0].country_id;
            getStates($location,'/acl/states/token/'+token+'/filters2/states.country_id/'+country_id+'/states.status/1',$scope,$http,true);


          }
        }
    });
}


function getUrls($location,url,$scope,$http,otherController = false){

    $http.get(url).success(function(urlsData) {

        var error = urlsData.error;
        if(error == 4){
         $location.path('/login/denegado');
        }

      $scope.urls = urlsData.urls;
      $scope.totalItemsUrls =urlsData.n_rows;

        if(!otherController){
          $scope.resource_select_id = 0;
          $scope.resource_select = '';
          if(urlsData.n_rows >= 1){
            $scope.formData.description = $scope.urls[0].description;
            $scope.formData.notes = $scope.urls[0].notes;
            $scope.formData.resource_id = $scope.urls[0].resource_id;
            $scope.formData.method = $scope.urls[0].method;

            $scope.application_id_select = $scope.urls[0].application_id;
            $scope.application_select = $scope.urls[0].application;

            $scope.resource_id_select = $scope.urls[0].resource_id;
            $scope.resource_select = $scope.urls[0].resource;

            getApplications($location,'/acl/applications/token/'+token+'/filter/applications.status/1',$scope,$http,true);
            var application_id = $scope.urls[0].application_id;
            getResources($location,'/acl/resources/token/'+token+'/filters2/resources.application_id/'+application_id+'/resources.status/1',$scope,$http,true);


          }
        }
    });
}


function asignFilter($scope){
    if($scope.filter==''){
        filter = -1;
    }
    else{
        filter = $scope.filter;

        patron = / /g;
        nuevoValor    = "|";

        filter = filter.replace(patron, nuevoValor);

    }
    return filter;
}
function updateTypeOrder($scope){
    if($scope.typeOrder == 'ASC'){
        $scope.typeOrder = 'DESC';
    }
    else{
        $scope.typeOrder = 'ASC';
    }
}

function asignColumn($scope){
    if($scope.column==''){
        column = -1;
    }
    else{
        column = $scope.column;
    }
    return column;
}

function showAlert($scope) {
    $scope.alertMessage =   {
        type: $scope.alertType,
        text: $scope.alertText,
        closable: $scope.closable,
        delay: $scope.secondsDelay
    };
}
